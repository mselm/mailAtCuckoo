import socket
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def is_analysis_running(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((ip,port))
    sock.close()

    """ Port is open
    """
    if result==0:
        logger.info(" Port {} with IP {} is open. Analysis is running".format(port, ip))
        return True

    else:
        logger.info(" Port {} with IP {} is close: No Analysis is running".format(port, ip))
        return False

