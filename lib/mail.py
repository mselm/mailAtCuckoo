import config
import os
import email
import time
import logging
import smtplib,imaplib

from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from socket import error as SocketError

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def build_mime_email(body, to, subject):
    outer = MIMEMultipart('related')
    outer["Subject"] = "Cuckoo Analyse-Ergebnis > Betreff: {}".format(subject)
    outer["From"] = config.email['analysis_address']
    outer["To"] = to
    outer["Cc"] = ", ".join(config.email["rcpts"])

    # wrap following parts in multipart/alternative mime
    #msgAlternative = MIMEMultipart('alternative')
    #outer.attach(msgAlternative)

    # text/plain part
    #msgText = MIMEText(body, 'plain')
    #msgAlternative.attach(msgText)

    # text/html text part
    msgHTML = MIMEText(body, 'html')
    outer.attach(msgHTML)
    #msgAlternative.attach(msgHTML)

    # img part
    img_data = open(os.path.join(config.cuckoo['base_dir'],'emailer/email_content/cuckoo.png'),"rb")
    img = MIMEImage(img_data.read(), 'png')
    img_data.close()
    img.add_header('Content-Id', '<cuckoo_logo>')
    img.add_header("Content-Disposition", "inline", filename="cuckoo_logo")
    outer.attach(img)

    return outer

def send_email(email):
    time.sleep(5)

    rcpts = list()
    rcpts.extend(config.email["rcpts"])
    rcpts.append(email["To"])
    print "response-recipients: {}".format(rcpts)

    try:
        s = smtplib.SMTP(config.email['smtp_server'], 587)
        s.ehlo()
        s.starttls()
        s.login(config.email['username'], config.email['password'])
        s.sendmail(config.email['analysis_address'],
                   rcpts,email.as_string())

        logger.info("Report successfully sent")
        return True

    except smtplib.SMTPException:
        logger.debug("failed sending mail: {}".format(traceback.print_exc()))
        return False
    except SocketError:
        logger.debug("Socket error. Sending mail failed!")
        return False

    finally:
        s.close()
