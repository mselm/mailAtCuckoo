import os
import threading
import logging
import config

from threading import Thread
from network.CuckooAPI import CuckooAPI
from Queue import Queue
from time import sleep
from db.database import Database
from emailer.Sample import UrlSample,CODE_OK
from helper.MISP import MISP


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class CuckooAnalyzer(Thread):

    def __init__(self,name,file_object):
        Thread.__init__(self)
        self.name = name
        self.file_object = file_object

    def run(self):

        if self.file_object.error_status == CODE_OK:
            self.file_object.load_report()

        if self.file_object.has_analyzed():
            logger.info("Fileobject has already been scanned: {}".format(self.file_object.filename))
            self.file_object.set_error_status(CODE_OK)

        elif self.file_object.error_status == CODE_OK and self.file_object.filename:
            task_id = CuckooAPI.send_to_cuckoo(self.file_object.filename)
            self.file_object.set_taskId(task_id)

            if not task_id > 0:
                logger.warn("Unable to start analysis. Error while setting taskId for {}".format(self.file_object.filename))
                return

            while not CuckooAPI.getReportStatus(self.file_object.taskId):
                logger.info("TaskId: {} filename: {} is being analyzed...\n".format(self.file_object.taskId, self.file_object.filename))
                sleep(20)

            """ When analysis status is 'reported', set report to file object """
            jData = CuckooAPI.getReport(task_id)

            if not jData:
                logger.warn("Unable getting jData for task_id: {}".format(task_id))
                return

            self.file_object.set_report(jData)
            self.file_object.execute_report_analyzer()

            """ HASH CASHING module: Register filehash and reference to reportfile
            for safing time when analyzing the same file next time """
            db = Database()

            hash = self.file_object.report_analyzer.results_map['meta']['md5']
            path = os.path.join(config.cuckoo['root_dir'],'storage/analyses',
                                    str(self.file_object.taskId),'reports/report.json')
            if hash and path:
                db.register_hash(hash,path)
                logger.info("Registered filehash {} successfully in DB".format(hash))
            else:
                logger.warn("Can not register filehash {} in DB".format(hash))

            """ MISP module: Insert founded IOCs in case of analyzed suspicioues file
            """
            if self.file_object.report_analyzer.scoring_map["totalScore"] > 3:
                if isinstance(self.file_object,UrlSample):
                    MISP().insert_iocs(self.file_object.report_analyzer.results_map,url=self.file_object.url)
                else:
                    MISP().insert_iocs(self.file_object.report_analyzer.results_map)
        else:
            logger.warn("Analysis can not be startet. Statuscode: {}, filename: {}".format(self.file_object.error_status, self.file_object.filename))

    @staticmethod
    def execute_cuckoo_analyzer(emails):
        threadpool = list()

        for mail in emails:
            files = mail.file_objects

            for file in files:
                thread = CuckooAnalyzer("Thread",file)

                """ Starts thread execution by calling run() method """
                thread.start()
                threadpool.append(thread)

        for thread in threadpool:

            """ blocks the calling thread until the thread
            whose join() method is called terminates """
            thread.join()

