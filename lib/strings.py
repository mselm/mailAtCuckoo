import email
import config
import hashlib


def get_md5(string):
    hash_string = hashlib.md5(string)
    return hash_string.hexdigest()

def get_domain(email_address):
    if '@' in email_address:
        pos = email_address.find('@')
        return email_address[pos:-1]

def remove_prefix(filename):
    return filename.split("_",1)[1]

def decode_string(filename):
    if filename is not None:
        filename = filename.replace("\r\n","")
        filename = filename.replace("\n","")
        
        try:
            filename = filename.encode('utf-8')
            text, encoding = email.Header.decode_header(filename)[0]
            text = unicode(text, errors='ignore')
            return text
        except UnicodeDecodeError:
            return ""
    return ""

def is_ascii(string):
    try:
        string.decode('ascii')
        return True
    except:
        return False

def decode_string2(string):
    if string is not None:
        string = unicode(string, errors='ignore')
        return string

def rename_file(filename):
    old_path = os.path.join(config.cuckoo['attachment_path'], filename)
    decoded_filename = "renamed_" + decode_string2(filename)
    new_path = os.path.join(config.cuckoo['attachment_path'], decoded_filename)

    os.rename(old_path, new_path)
    return decoded_filename

def strip(string):
    if '+' in string and not '?' in string:
        pos = string.find('+')
        return string[:pos-1]
    return string
