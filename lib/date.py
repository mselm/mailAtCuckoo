import datetime
from lib.strings import strip


def get_date():
    return datetime.datetime.now().strftime("%Y-%b-%d %H:%M:%S")

def format_date(date):
    date = strip(date)
    try:
        return datetime.datetime.strptime(date,'%a, %d %b %Y %H:%M:%S').strftime('%Y-%b-%d %H:%M:%S')

    except ValueError:
        return str(date)
