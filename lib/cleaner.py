import os
import time
import config
import logging

from helper.Process import execute_shell_cmd


LIMIT_FREE_DISCSPACE= 5368709120

logging.basicConfig(level=config.logging_level)
logger = logging.getLogger(__name__)


class Cleaner(object):

    @classmethod
    def start_clearing(cls):
        cls.exec_cuckoo_clean()
        cls.del_logfiles()
        cls.del_stored_attachments()

    @classmethod
    def exec_cuckoo_clean(cls):
        execute_shell_cmd(["cuckoo", "clean"])
        time.sleep(4)
        logger.info( "Executed cuckoo clean command successfully" )

    @classmethod
    def del_logfiles(cls):
        if os.path.isfile("dump.info"):
            execute_shell_cmd(["rm", "dump.info"])
            time.sleep(2)
            logger.info( "Deleted dump.info file successfully" )

        if os.path.isfile("error.info"):
            execute_shell_cmd(["rm", "error.info"])
            time.sleep(2)
            logger.info( "Deleted error.info file successfully" )



    @classmethod
    def del_stored_attachments(cls):
        execute_shell_cmd(["rm", "-r", "{}".format(os.path.join(config.cuckoo['attachment_path']), '*')])
