import hashlib


def get_md5(path):
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(path, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()

def calculate_dir_size(dir):
    common_dirsize = 0
    for (dirpath, dirnames, filenames) in os.walk(dir):
        for filename in filenames:
            common_dirsize += os.path.getsize(os.path.join(dirpath, filename))
    return common_dirsize

def check_filetype_without_zip(filename):
    if(filename.lower().endswith(('.py','.js', '.doc', '.docx', '.docm', '.xls',
                                  '.xlsx', '.xlsm', '.ppt', '.pptx', '.pptm', '.pdf',
                                  '.file', '.com', '.exe'
                                ))):
        return True
    return False

""" Zip-type included
"""
def is_legal_filetype(filename):
    return filename.lower().endswith(('.txt', '.doc', '.docx',
                           '.docm', '.xls', '.xlsx', '.xlsm',
                           '.ppt', '.pptx', '.pptm', '.pdf',
                           '.zip', '.exe', '.py', '.js',
                           '.file', '.com'))

def is_zip(filename):
    if(filename.lower().endswith(('.zip'))):
        return True
    return False

def get_legal_filetypes():
    legalFileTypes = [
                     ".fon",".db", ".manifest",
                     ".lst", ".png", ".bud", ".deu", ".ttc",
                     ".TTC", ".ttf", ".pdf",
                     ".gpd", ".dic", ".sdb", ".nls" ,
                     ".config", ".csv", ".tmp", ".ini",
                     ".dotm", ".lex", ".dub", ".dll",
                     ".doc", ".docx", ".docm", ".xls", ".xlsx", ".xlsm",
                     ".ppt", ".pptx", ".pptm", ".rcd", ".dat", ".cvr",
                     ".xml", ".wmf"
    ]
    return legalFileTypes

