""" Configuration Module """

###################################   NOTE   ############################################
#
# This is the configuration file for the mail2Cuckoo addon. Please replace the fields
# in brackets with your configuration. Read README.md for additional information
# Cuckoo Sandbox and Mail2Cuckoo are written in Python2.7. Mail2Cuckoo has been optimized
# for file analysis with virtualbox vm`s in a non distributed analysis environment.
#
#########################################################################################


import os
import logging


#Change this value to the location of the tool, currently its a subdirectory of HOME
BASE_DIR = os.path.join(os.getenv("HOME"),"<your/path/to>/mail2Cuckoo")

logging_level = logging.INFO

cuckoo = dict(

    #This is default (see https://cuckoo.sh/docs/installation/host/cwd.html)
    root_dir = os.path.join(os.getenv("HOME"),".cuckoo"),

    base_dir = BASE_DIR,

    #Stores all downloaded attachments and URL referenced files here
    attachment_path = os.path.join(BASE_DIR,'attachments'),

    #Necessary for status checks of analysis machines. The values
    #should be the same values as [machines] in $CWD/conf/virtualbox.conf)
    machines = ['<machine1>','<machine2>']
)

#Port the Cuckoo Sandbox REST API should listen to
port_api = 5001

#Definition of static request-URLs, which allows guest communication
#( see https://cuckoo.sh/docs/usage/api.html )
rest = dict(
    create_file = 'http://localhost:{}/tasks/create/file'.format(port_api),
    tasks_report = 'http://localhost:{}/tasks/report/'.format(port_api),
    cuckoo_status = 'http://localhost:{}/cuckoo/status'.format(port_api),
    get_pcap = 'http://localhost:{}/pcap/get/'.format(port_api),
    file_view = 'http://localhost:{}/files/view/id/'.format(port_api),
    report_status = 'http://localhost:{}/tasks/view'.format(port_api),
    machines_view = 'http://localhost:{}/machines/view'.format(port_api),
)

#The resultserver(=host)-config should be consistent with resultserver section in
#$CWD/conf/cuckoo.conf
resultserver = dict(

    # Must be the same value as [resultserver] in $CWD/conf/cuckoo.conf
    ip = '192.168.56.1',
    port = '2042',
    netmask = '255.255.255.0',
)

#Mailserver configuration the tool should connect to. The script fetches eMails over a IMAP4-over-SSL
#connection (Port 993 is used).
#Reports-E-Mails are sent over SMTP(S) on Port 587. Depending on the provider and its mailserver please
#adjust emailer/MailboxHandler.py (check docs.python.org/2/library).
email = dict(

    #Dedicated E-mail-address for analysis purpose
    analysis_address = '<analysis_email_address>',

    #Server credentials
    imap_server = '<imap_server>',
    smtp_server = '<smtp_server>',
    username = '<username>',
    password = '<password>',

    #Mailbox storing analysis emails
    mailbox = 'INBOX',

    #Name of mailbox storing already reported eMails. Make sure that the mailbox has
    #already been created at the mailserver
    mailbox_reported_mails = '<backup_mailbox>',

    #Define recipients in addition to Email[From] for receiving analysis results
    rcpts = ['<email_address1>','<email_address2>'],
    sys_admin = '<email_address>',
)

#Install postgreSQL and create a new DB (check README.md 'Installation Datenbank' for this). Add the database name and username to dbname- and user-fields.
#ATTENTION: Create a different database for this tool as in cuckoo sandbox.
postgres = dict(
    con = 'postgresql://<role>:<password>@localhost:5432/<dbname>'
)

#The URL-analyze-mechanism is based on virustotal. Mail2Cuckoo sends URLs to virustotal as plain text and
#fetches existing analysis-results. If the specific URL has not been analyzed yet, the script skips this url.
#Set your virustotal key here to add the URL scan functionality. For a key create a free account at virustotal.
#Keep in mind, that a commercial use is not allowed. For more information
#see https://www.virustotal.com/de/documentation/public-api/
virustotal = dict(
    key = '<virustotal_key>',
)

#MISP allows you sharing IOCs (indicators of compromise) and import them into other components (e.g
#active firewalls). Currently just HTTP connection is supported.
misp = dict(
    url = "<host_url>",
    key = "<auth_key>",
)

""" END Configuration Module """
