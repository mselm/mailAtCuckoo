import json
import requests
import time
import config
import logging


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class VirusTotalObject(object):

    def __init__(self, tmp_url, final_url):
        self.tmp_url = tmp_url
        self.final_url = final_url
        self.report = None
        self.num_positives = 0
        self.spam_list = list()

    def check_infection_state(self):
        logger.info("Checking url infection state of {}...".format(self.tmp_url))
        self.report = API.get_json_report(self.tmp_url)

        if not self.report:
            return

        if self.tmp_url != self.final_url and not 'positives' in self.report:
            logger.info( "tmp url:{}".format(self.tmp_url) )
            logger.info( "final url:{}".format(self.final_url) )
            logger.info("Checking url infection state of {}...".format(self.final_url))
            self.report = API.get_json_report(self.final_url)

        if self.report:
            if 'response_code' in self.report and 'positives' in self.report:
                self.num_positives += self.report['positives']


class API(object):

    @staticmethod
    def get_json_report(url):
        params = {'apikey': config.virustotal["key"], 'resource':url}
        headers = {
                  "Accept-Encoding": "gzip, deflate",
                  "User-Agent" : "gzip,  Cuckoo"
        }
        try:
            response = requests.post('https://www.virustotal.com/vtapi/v2/url/report',timeout=5,
                params=params, headers=headers)

        except requests.exceptions.ConnectTimeout:
            logger.info( "Connect timeout to virustotal server" )
            return

        except requests.exceptions.ReadTimeout:
            logger.info( "Read timeout to virustotal server" )
            return

        """ Four requests per minute allowed """
        time.sleep(25)
        json = None
        try:
            json = response.json()

        except ValueError:
            logger.info( "No Json object could be decoded" )

        return json
