import subprocess
import logging
import config


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class VBInterface(object):

    registered_machines = config.cuckoo['machines']

    def __init__(self):
        pass

    """ Query current machine status. valid status arguments are
    'saved','running','powered off'
    """
    @classmethod
    def check_machine_status(cls,status):
        if not cls.registered_machines:
            logger.info("No registered machine found in config")
            return False

        ready_machines = list()

        for machine in cls.registered_machines:

            try:
                vminfo = subprocess.Popen(('vboxmanage','showvminfo','{}'.format(machine)), stdout=subprocess.PIPE)
                count = subprocess.check_output(('grep', '-c', '{}'.format(status)), stdin=vminfo.stdout)
                vminfo.wait()

                if int(count) == 1:
                    ready_machines.append(machine)
                    logger.info("Machine '{}' ready".format(machine))

                else:
                    logger.info("Machine '{}' is not ready".format(machine))

            except subprocess.CalledProcessError:
                logger.info("Machine '{}' is not ready".format(machine))
                continue

        if len(ready_machines) > 0:
            return True

        return False
