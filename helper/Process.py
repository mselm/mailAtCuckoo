import signal
import ConfigParser
import os
import time
import subprocess
import logging
import config

from subprocess import Popen


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def execute_shell_cmd(cmd):
    subprocess.call(cmd)

class CuckooProcessManager:

    vboxmanage_command = ['VBoxManage', 'hostonlyif', 'ipconfig', 'vboxnet0', '--ip', '{}'.format(config.resultserver['ip']), '--netmask', '{}'.format(config.resultserver['netmask'])]

    cuckoo_deamons = {
               "Cuckoo_main_deamon":['/usr/local/bin/cuckoo'],
               "Cuckoo_api_deamon":['/usr/local/bin/cuckoo', 'api', '--host', '0.0.0.0', '--port', '{}'.format(config.port_api)]
    }

    def __init__(self):
        self.process_list = list()

    def __enter__(self):
        """ Setting up default virtual interface
        """
        execu = Popen(self.vboxmanage_command, stdout=subprocess.PIPE)
        out = execu.communicate()

        """ Starting cuckoo main deamon and cuckoo api
        """
        for deamon,command in self.cuckoo_deamons.iteritems():
            try:
                logger.info("Starting {}".format(deamon))
                proc = Popen(command, stdout=subprocess.PIPE, preexec_fn=os.setsid)
                self.process_list.append(proc)
                time.sleep(7)

            except os.error as e:
                logger.warn("Error while create process: {}".format(e))

    def __exit__(self, type, value, traceback):
        for process in self.process_list:
            time.sleep(3)
            try:
                os.killpg(os.getpgid(process.pid), signal.SIGTERM)
                logger.info(" Pid {} has been killed".format(str(process.pid)))

            except os.error as e:
                logger.warn("Error while killing process: {}".format(e))
