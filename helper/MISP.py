import config
import logging

from pymisp import PyMISP
from pymisp.exceptions import PyMISPError

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class MISP(object):

    def __init__(self):
        pass

    @staticmethod
    def insert_iocs(report,url=None):
        if not config.misp['url'] or not config.misp['key']:
            logger.info("MISP module is not active. Please add host url and/or key in config.py")
            return

        try:
            api = PyMISP(config.misp['url'],config.misp['key'])
            event = api.new_event(0,1,0,info="Cuckoo Analysis",published=True)

            if report['network']['domains'] or report['network']['connects_host'] or report['network']['tcpUdpCon']:
                activities = set()
                activities.update(report['network']['domains'])
                activities.update(report['network']['connects_host'])
                activities.update(report['network']['tcpUdpCon'])

                for a in activities:
                    event = api.add_ipsrc(event,a,category="Network activity")

            if report['meta']['filename']:
                event = api.add_hashes(event,category="Artifacts dropped",
                                             filename=report['meta']['filename'],
                                             md5=report['meta']['md5'],
                                             sha1=report['meta']['sha1'],
                                             sha256=report['meta']['sha256']
                )

            if url:
                event = api.add_url(event,url,category="Network activity")

        except PyMISPError:
            logger.info("Error with MISP connection.")

    @staticmethod
    def insert_url(url,ip=""):
        if not config.misp['url'] or not config.misp['key']:
            logger.info("MISP module is not active. Please add host url and/or key in config.py")
            return
        try:
            api = PyMISP(config.misp['url'],config.misp['key'])
            event = api.new_event(0,1,0,info="Cuckoo Analysis",published=True)

            if url:
                event = api.add_url(event,url,category='Network activity',comment='Malicious URL in E-Mail-Body')
            if ip:
                event = api.add_ipsrc(event,ip,category="Network activity")

        except PyMISPError:
            logger.info("Error with MISP connection")
