from report.analyzingModules.AnalyzingModules import AnalyzingModules


class Signatures(AnalyzingModules):

    def __init__(self):
        super(Signatures, self).__init__()
        self.json_signs = None

    def find_malware_traces(self):
        self.json_signs = self.jsonReport.get_json_cat('signatures')

        if self.json_signs:
            return self.checkReport()

    def checkReport(self):
        signatures = dict()
        descriptions = list()

        for index, key in enumerate(self.json_signs):
            if 'description' in key:
                descriptions.append(key['description'])

            if key['marks']:
               for innerIndex, innerKey in enumerate(key['marks']):
                   if 'category' in innerKey:
                       isCmd = innerKey['category']

                       if 'cmdline' in isCmd:
                           descriptions.append(innerKey['ioc'])

        return descriptions
