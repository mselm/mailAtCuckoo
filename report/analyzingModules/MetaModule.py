from report.analyzingModules.AnalyzingModules import AnalyzingModules


class MetaModule(AnalyzingModules):

    def __init__(self):
        super(MetaModule, self).__init__()
        self.json_meta = None

    def find_malware_traces(self):
        return self.checkReport()

    def checkReport(self):
        metaList = dict()

        json_target = self.jsonReport.get_json_cat('target')

        if 'file' in json_target:
            self.json_meta = json_target['file']

            metaList['filename'] = self.getFilename()
            metaList['type'] = self.getType()
            metaList['md5'] = self.getMd5()
            metaList['sha1'] = self.getSha1()
            metaList['sha256'] = self.getSha256()
            metaList['sha512'] = self.getSha512()
            metaList['taskId'] = "-"

        return metaList

    def getFilename(self):
        return self.json_meta['name'] if self.json_meta else "n/a"

    def getType(self):
        return self.json_meta['type'] if self.json_meta else "n/a"

    def getMd5(self):
        return self.json_meta['md5'] if self.json_meta else "n/a"

    def getSha1(self):
        return self.json_meta['sha1'] if self.json_meta else "n/a"

    def getSha256(self):
        return self.json_meta['sha256'] if self.json_meta else "n/a"

    def getSha512(self):
        return self.json_meta['sha512'] if self.json_meta else "n/a"

