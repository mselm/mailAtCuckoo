from report.analyzingModules.AnalyzingModules import AnalyzingModules


class StringsModule(AnalyzingModules):

    def __init__(self):
        super(StringsModule, self).__init__()
        self.json_strings = None

    def find_malware_traces(self):
        self.json_strings = self.jsonReport.get_json_cat('strings')

        if self.json_strings:
            return self.checkReport()

    def checkReport(self):
        stringHits = dict()
        stringHits['malwareIndicators'] = self.getIndicators()
        stringHits['is_encrypted'] = self.is_encrypted_office()

        return stringHits

    def getIndicators(self):
        malwareIndicators = {"/OpenAction" : "File executes script automatically",
                             "/AA": "File executes script automatically",
                             "/Names": "File possibly executes script",
                             "/AcroForm": "File executes script automatically",
                             "/Action": "File executes script automatically",
                             "/JavaScript": "File executes javascript",
                             "/Launch" : "File executes another program",
                             "/URI": "File opens an internet ressource",
                             "/SubmitForm": "File attempts to establish connection to URL",
                             "/GoToR": "File attempts to establish connection to URL",
                             "/RichMedia": "Found embedded flash in PDF",
                             "word/vbaProject.bin": "File contains VisualBasic Skript",
                             "xl/vbaProject.bin": "File contains Visual Basic Skript",
                             "ppt/vbaProject.bin": "File contains Visual Basic Skript",
                             "_VBA_PROJECT": "File contains Visual Basic Skript"
        }
        hits = set()
        for key in malwareIndicators:
            if key in self.json_strings:
                hits.add("Auffaelligkeit gefunden: {} {}".format(key, malwareIndicators[key]))

        return hits

    def is_encrypted_office(self):
        keywords = [
            "Microsoft.Container.EncryptionTransform",
            "StrongEncryptionDataSpace",
            "EncryptionInfo",
            "StrongEncryptionTransform",
            "Cryptographic Provider",
        ]
        keyword_occ = 0

        for key in keywords:
            if any(key in s for s in self.json_strings):
                keyword_occ += 1

        if keyword_occ > 2:
            return "True"
        return "False"
