from report.analyzingModules.AnalyzingModules import AnalyzingModules


class VirusTotalModule(AnalyzingModules):

    def __init__(self):
        super(VirusTotalModule, self).__init__()
        self.json_virustotal = None

    def find_malware_traces(self):
        self.json_virustotal = self.jsonReport.get_json_cat('virustotal')

        if self.json_virustotal:
            return self.checkReport()

    def checkReport(self):
        detectingEngines = list()

        if 'scans' in self.json_virustotal:

            for key,value in self.json_virustotal['scans'].iteritems():
                value = value['detected']
                if value:
                    detectingEngines.append(key)
        return detectingEngines
