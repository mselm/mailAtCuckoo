from report.analyzingModules.AnalyzingModules import AnalyzingModules


class YaraModule(AnalyzingModules):

    def __init__(self):
        super(YaraModule, self).__init__()
        self.json_buffer = None

    def find_malware_traces(self):
        self.json_buffer = self.jsonReport.get_json_cat('buffer')
        if self.json_buffer:
            return self.checkReport()

    def checkReport(self):
        yaraDetection = set()

        for index, key in enumerate(self.json_buffer):
            if report['buffer'][index]['yara']:

                if(len(report['buffer'][index]['yara']) > 1):
                    for iindex, kkey in enumerate(report['buffer'][index]['yara']):
                        ssuspString = report['buffer'][index]['yara'][iindex]['strings']
                        ssuspDescription = report['buffer'][index]['yara'][iindex]['meta']['description']
                        hit = "Auff&auml;lliges Muster: {} ({})".format(ssuspDescription, ssuspString)
                        yaraDetection.add(hit)
                else:
                    suspString = report['buffer'][index]['yara'][0]['strings']
                    description = report['buffer'][index]['yara'][0]['meta']['description']
                    hit = "Auff&auml;lliges Muster: {} ({})".format(description, suspString)
                    yaraDetection.add(hit)

            return yaraDetection
