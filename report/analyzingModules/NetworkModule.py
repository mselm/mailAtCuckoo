import getpass

from report.analyzingModules.AnalyzingModules import AnalyzingModules


class NetworkModule(AnalyzingModules):

    def __init__(self):
        super(NetworkModule, self).__init__()
        self.json_summary = None
        self.json_network = None

    def find_malware_traces(self):
        return self.checkReport()

    def checkReport(self):
        self.json_network = self.jsonReport.get_json_cat('network')
        self.json_summary = self.jsonReport.get_json_behave_summary()
        networkMap = dict()

        if self.json_summary:
            networkMap['connects_host'] = self.getConnectedHosts()
        if self.json_summary and self.json_network:
            networkMap['tcpUdpCon'] = self.getConnections()
        if self.json_network:
            networkMap['domains'] = self.getDomains()

        return networkMap

    def getConnectedHosts(self):
        connectedHosts = set()
        cur_username = getpass.getuser().lower()

        if 'connects_host' in self.json_summary:
            for entry in self.json_summary['connects_host']:
                if not cur_username in entry and not 'wpad' in entry.lower():
                    connectedHosts.add(entry)

        if 'resolves_host' in self.json_summary:
            for resolvedHost in self.json_summary['resolves_host']:
                if not cur_username in resolvedHost and not 'wpad' in resolvedHost.lower():
                    connectedHosts.add(resolvedHost)
        if self.json_network:
            if 'hosts' in self.json_network:
                for key in self.json_network['hosts']:
                    if not cur_username in key and not 'wpad' in key.lower():
                        connectedHosts.add(key)

        return connectedHosts

    def getDomains(self):
        whitelist = [o.name for o in self.db.select('domain')]
        requestedDomains = set()
        if 'domains' in self.json_network:
            for key in self.json_network['domains']:
                if not key['domain'] in whitelist:
                    requestedDomains.add(key['domain'])

        return requestedDomains

    def getConnections(self):
        occuredConn = set()
        whitelist = [o.ip for o in self.db.select('ip')]

        for key in self.json_network['udp']:
            if not key['src'] in whitelist and not key['dst'] in whitelist:
                occuredConn.add(key['src'])
                occuredConn.add(key['dst'])

        for key in self.json_network['tcp']:
            if not key in whitelist:
                occuredConn.add(key['src'])
                occuredConn.add(key['dst'])

        if 'connects_ip' in self.json_summary:
            for entry in self.json_summary['connects_ip']:
                if not entry in whitelist:
                    occuredConn.add(entry)

        return occuredConn
