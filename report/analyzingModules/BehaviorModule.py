from report.analyzingModules.AnalyzingModules import AnalyzingModules
from db.database import Database
from lib.file import get_legal_filetypes


class BehaviorModule(AnalyzingModules):

    sections = (
               'file_opened',
               'file_written',
               'file_deleted',
               'directory_enumerated',
               'process',
               #'buffer_command'
    )

    def __init__(self):
        super(BehaviorModule, self).__init__()

    def find_malware_traces(self):
        return self.checkReport()

    def checkReport(self):
        behaviorMap = dict.fromkeys(self.sections)

        behaviorMap['process'] = self.getProcesses()
        behaviorMap['file_opened'] = self.getOpenedFiles()
        behaviorMap['file_written'] = self.getWrittenFiles()
        behaviorMap['file_deleted'] = self.getDeletedFiles()
        behaviorMap['directory_enumerated'] = self.getDirectories()
        #behaviorMap['buffer_command'] = self.getBufferScript()

        return behaviorMap

    def getBufferScript(self):
        report = self.getReport()
        bufferList = list()

        if report is not None:
            if 'processes' in report['behavior']:
                for key, value in enumerate(report['behavior']['processes']):
                    if report['behavior']['processes'][key]['calls']:
                        for innerKey, innerValue in enumerate(report['behavior']['processes'][key]['calls']):
                            if report['behavior']['processes'][key]['calls'][innerKey]['arguments']:
                                if 'buffer' in report['behavior']['processes'][key]['calls'][innerKey]['arguments']:
                                    if 'http://' in report['behavior']['processes'][key]['calls'][innerKey]['arguments']['buffer']:
                                        bufferList.append(report['behavior']['processes'][key]['calls'][innerKey]['arguments']['buffer'])
                                    elif 'https://' in report['behavior']['processes'][key]['calls'][innerKey]['arguments']['buffer']:
                                        bufferList.append(report['behavior']['processes'][key]['calls'][innerKey]['arguments']['buffer'])
        print "bufferList: {}".format(bufferList)

    def getProcesses(self):
        whitelist = [o.path for o in self.db.select('process')]
        invokedProcesses = set()

        for process in self.jsonReport.get_json_processes():
            if process['process_path'] not in whitelist:
                invokedProcesses.add(process['process_path'])

        return invokedProcesses

    def getOpenedFiles(self):
        openedFiles = self.jsonReport.get_json_opened_files()
        suspOpenedFiles = set()

        if openedFiles:
            for file in openedFiles:
                file= file.lower()

                #Check files (not path) and ignore pdf version with .0
                if("." in file and ".0" not in file):
                   if not(any(filetype in file.lower() for filetype in get_legal_filetypes())):
                       if not("c:\\python27" in file) and ("system32" in file and ".exe" in file and not "profile.icm" in file):
                           suspOpenedFiles.add(file)
                   else:
                       continue

        return suspOpenedFiles

    def getWrittenFiles(self):
        writtenFiles = self.jsonReport.get_json_written_files()
        suspWrittenFiles = set()

        if writtenFiles:
            for file in writtenFiles:
                if not(any(filetype in file.lower() for filetype in get_legal_filetypes())) and not ("Roaming\Adobe\Acrobat" in file)\
                    and not "\AppData\Roaming\Oracle" in file:
                    suspWrittenFiles.add(file)

        return suspWrittenFiles

    def getDeletedFiles(self):
        deletedFiles = self.jsonReport.get_json_deleted_files()
        suspDeletedFiles = set()

        if deletedFiles:
            suspDeletedFiles = [s for s in deletedFiles if ".xml" not in s]

        return suspDeletedFiles

    def getDirectories(self):
        directoryEnum = self.jsonReport.get_json_dirs()
        suspDirectories = set()
        if directoryEnum:
            for dir in directoryEnum:
                if (".exe" in dir.lower() and "appdata" in dir.lower()) and not ("\AppData\Roaming\Oracle\bin" in dir):
                    suspDirectories.add(dir)

        return suspDirectories
