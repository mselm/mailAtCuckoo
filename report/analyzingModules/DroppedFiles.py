from report.analyzingModules.AnalyzingModules import AnalyzingModules
from lib.strings import remove_prefix


class DroppedFiles(AnalyzingModules):

    def __init__(self):
        super(DroppedFiles, self).__init__()

    def find_malware_traces(self):
        return self.checkReport()

    def checkReport(self):
        droppedList = list()
        dropped_files = self.jsonReport.get_json_cat('dropped')

        if dropped_files:
            for index, key in enumerate(dropped_files):

                if dropped_files[index]['name']:
                    filename = dropped_files [index]['name']

                    if not(".lst" in filename) and not("shareddataevents" in filename):
                        droppedList.append(remove_prefix(filename))
        return droppedList
