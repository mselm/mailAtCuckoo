from report.analyzingModules.AnalyzingModules import AnalyzingModules


class StaticModule(AnalyzingModules):

    def __init__(self):
        super(StaticModule, self).__init__()

    def find_malware_traces(self):
        return self.checkReport()

    def checkReport(self):
        report = self.getReport()

        if report is not None:
            statics = dict()
            if 'static' in report:
                if 'office' in report['static']:
                    if 'macros' in report['static']['office']:
                        for index, key in enumerate(report['static']['office']['macros']):
                            statics['stream']=report['static']['office']['macros'][index]['stream']
                            statics['filename']=report['static']['office']['macros'][index]['filename']

                if 'pdf' in report['static']:
                    pass

            return statics
