from report.analyzingModules.AnalyzingModules import AnalyzingModules
from report.analyzingModules.VirusTotalModule import VirusTotalModule
from report.analyzingModules.BehaviorModule import BehaviorModule
from report.analyzingModules.StaticModule import StaticModule
from report.analyzingModules.Signatures import Signatures
from report.analyzingModules.DroppedFiles import DroppedFiles
from report.analyzingModules.NetworkModule import NetworkModule
from report.analyzingModules.StringsModule import StringsModule
from report.analyzingModules.YaraModule import YaraModule
from report.analyzingModules.MetaModule import MetaModule
from report.Report import Report
from report.Scoring import Scoring


class ReportAnalyzer:

    def __init__(self, jData):
        self.report = Report(jData)
        self.results_map = dict()

    def analyze(self):
        self.analyze_report()
        self.calculate_scores()

    def analyze_report(self):
        AnalyzingModules.setReport(self.report)

        virus_total = VirusTotalModule()
        self.results_map['virustotal'] = virus_total.find_malware_traces()

        signature = Signatures()
        self.results_map['signatures'] = signature.find_malware_traces()

        statics = StaticModule()
        self.results_map['static'] = statics.find_malware_traces()

        dropped_files = DroppedFiles()
        self.results_map['dropped'] = dropped_files.find_malware_traces()

        behavior = BehaviorModule()
        self.results_map['behavior'] = behavior.find_malware_traces()

        network = NetworkModule()
        self.results_map['network'] = network.find_malware_traces()

        strings = StringsModule()
        self.results_map['strings'] = strings.find_malware_traces()

        yara = YaraModule()
        self.results_map['yara'] = None

        meta = MetaModule()
        self.results_map['meta'] = meta.find_malware_traces()
        print "results map"
        print self.results_map

    def calculate_scores(self):
        if self.results_map:
            self.scoring_map = Scoring.get_scoring_map(self.results_map)

    def get_suspicious_entries(self):
        return self.results_map
