import report.Report
from db.database import Database


class AnalyzingModules(object):

    jsonReport = None

    def __init__(self):
        self.db = Database()

    def getReport(self):
        return type(self).jsonReport.getReport()

    @classmethod
    def setReport(cls, report):
        AnalyzingModules.jsonReport = report
