class Report:

    def __init__(self, jData):
        self.report = jData

    def getReport(self):
        return self.report

    def get_json_cat(self, cat):
        if cat in self.report:
            return self.report[cat]

    def get_json_processes(self):
        if self.report:
            json_behavior = self.get_json_cat('behavior')

            if 'processes' in json_behavior:
                return json_behavior['processes']

    def get_json_behave_summary(self):
        if self.report:
            json_behavior = self.get_json_cat('behavior')

            if 'summary' in json_behavior:
                return json_behavior['summary']

    def get_json_opened_files(self):
        summary = self.get_json_behave_summary()
        if summary:
            return summary['file_opened'] if 'file_opened' in summary else None

    def get_json_written_files(self):
        summary = self.get_json_behave_summary()
        if summary:
            return summary['file_written'] if 'file_written' in summary else None

    def get_json_deleted_files(self):
        summary = self.get_json_behave_summary()
        if summary:
            return summary['file_deleted'] if 'file_deleted' in summary else None

    def get_json_dirs(self):
        summary = self.get_json_behave_summary()
        if summary:
            return summary['directory_enumerated'] if 'directory_enumerated' in summary else None

