from db.database import Database
from report.Report import Report


class Scoring:

    db = Database()

    @classmethod
    def get_scoring_map(cls,results_map):
        scores = dict()
        scores['signatures'] = cls.get_signs_score(results_map['signatures'])
        scores['virustotal'] = cls.get_virustotal_score(results_map['virustotal'])
        scores['network'] = cls.get_network_score(results_map['network'])
        scores['strings'] = cls.get_string_score(results_map['strings'])
        scores['behavior'] = cls.get_behavior_score(results_map['behavior'])

        scores['totalScore'] = cls.get_total_score(scores)
        return scores

    @classmethod
    def get_total_score(cls,scores):
        total_score = 0
        for key,value in scores.iteritems():
            if "strings" not in key:
                total_score += value
        if total_score < 2:
            return 0
        elif total_score > 10:
            return 10

        return total_score

    @classmethod
    def get_behavior_score(cls,signs):
        score = 0
        if signs['process']:
            score += len(signs['process']) * cls.db.get_score('process')
        if signs['file_opened']:
            score += len(signs['file_opened']) * cls.db.get_score('opened_files')
        if signs['file_written']:
            score += len(signs['file_written']) * cls.db.get_score('written_files')
        if signs['file_deleted']:
            score += len(signs['file_deleted']) * cls.db.get_score('deleted_files')
        if signs['directory_enumerated']:
            score += len(signs['directory_enumerated']) * cls.db.get_score('dir_enum')
        return score

    @classmethod
    def get_signs_score(cls,signs):
        score = 0
        if signs:
            sigScore = cls.db.get_score('sig')
            score = len(signs) * sigScore
        return score

    @classmethod
    def get_virustotal_score(cls,virustotal):
        score = 0
        if virustotal:
            for hit in virustotal:
                score += cls.db.get_virustotal_score(hit.lower())
        return score

    @classmethod
    def get_network_score(cls,network):
        score = 0
        if network:
            if 'domains' in network:
                score += len(network['domains']) * cls.db.get_score('domain')
            if 'tcpUdpCon' in network:
                score += len(network['tcpUdpCon']) * cls.db.get_score('tcp')
            if 'connects_host' in network:
                score += len(network['connects_host']) * cls.db.get_score('host')
        return score

    @classmethod
    def get_string_score(cls,strings):
        print strings
        score = 0
        if strings:
            if strings['malwareIndicators']:
                return len(strings) * cls.db.get_score('string')
        return score
