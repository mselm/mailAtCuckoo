# Mail2Cuckoo

Das Plugin erweitert das kommandozeilen-basierte Analysetool Cuckoo Sandbox um eine E-Mail-Schnittstelle. Nach der Analyse der E-Mail-Anhänge bzw. im Payload enthaltener URLs erhält der Benutzer eine scorebasierte Gefahreneinschätzung in Form eines E-Mail-Reports.

![alt Mail2Cuckoo Reports](https://www.web-up2date.de/cuckoo_reports.png)

## Erste Schritte

Die folgenden Informationen helfen Ihnen, das Analysetool einzurichten und zu installieren. Bitte lesen Sie die Anweisungen aufmerksam durch.

### Vorbereitungen

Dateianalysen werden von Cuckoo Sandbox durchgeführt. Bitte setzen Sie zunächst ein Cuckoo Analysenetz auf. Naehere Informationen hierzu finden Sie in der offiziellen Cuckoo [Dokumentation] (https://cuckoo.sh/docs/).
1. Das Plugin für Oracle VM's (VirtualBox) optimiert
2. Erstellen Sie nach dem Aufbau der Cuckoo Sandbox eine SQL-DB (das Plugin ist für POSTGRESQL optimiert)

### Installation Datenbank

Zunaechst erstellen Sie die DB und legen Sie einen Benutzer an

```
$ pip install psycopg2
```

Erstellen Sie nun die Rolle <rolename> und legen Sie die Datenbank <datenbank>

```
$ sudo -u postgres psql
postgres=# CREATE ROLE <rolename> with CREATEDB LOGIN ENCRYPTED PASSWORD '<password>';
postgres=# CREATE DATABASE <database> OWNER <rolename>;
```

Vergessen Sie hinter den SQL-Anweisungen das abschliessende Semikolon nicht.

Die folgendenen Befehle zeigen nuetzliche Informationen an.

```
postgres=# \du //Anzeige aller Rollen mit Attributen
postgres=# \dt //Anzeige aller erstellten Tables
postgres=# \l  //Anzeige aller Datenbanken mit weiteren Informationen
```

Mehr Informationen erhalten Sie in der offiziellen [Dokumentation] (https://www.postgresql.org/docs).

Sie koennen nun die gerade erstellte Datenbank in die $CWD/conf/cuckoo.conf in der [database]-Sektion eintragen.

### Installation mail2Cuckoo

Das Plugin analysiert E-Mails aus einem dedizierten Mailpostfach. Erstellen Sie fuer diesen Zweck ein eigenes Postfach auf Ihrem Mailserver.

Wechseln Sie anschliessend in das HOME-Verzeichnis und speichern Sie den Code

```
$ cd ~
$ git clone https://gitlab.mpcdf.mpg.de/mselm/mailAtCuckoo
```

Editieren Sie innerhalb der config.py Datei alle Platzhalter (eckigen Klammern).

Vergessen Sie nicht die Erstellung eines Ordners, um die Analysedateien abzulegen. Erstellen Sie den Ordner in dem Verzeichnis, indem die main.py-Datei liegt. Standardmaessig ist das $HOME/mailAtCuckoo.

```
$ cd ~/mailAtCuckoo
$ mkdir attachments
```

Bevor Sie das Tool ausfuehren, sollten Sie die Default-Eintraege der Whitelisttables in db/database.py ueberpruefen. Nehmen Sie dazu die IP-Adressen und Domains auf, die Sie fuer unbedenklich halten. Ueberpruefen Sie zusätzlich die Prozesspfade in process_wl. Je nach VM, installiertem OS und installierter Software muessen Sie hier die Eintraege ergaenzen bzw. berichtigen.   
Wenn Sie in der Reportnachricht ein zu hohes Scoring feststellen, ueberpruefen Sie nochmals Ihre Whitelists und achten Sie darauf, dass die eingetragenen Werte übernommen werden. Mit dem CLI-Tool von postgres koennen Sie auch manuell auf Ihre DB zugreifen.

Es empfiehlt sich die Einrichtung eines cronjobs, um das Skript im festgelegten Intervall ausfuehren zu lassen

```
*/5 * * * * python <your/path/to>/main.py >> <your/path/to>/dump.info 2>&1
```

Die Anweisung fuehrt das Skript alle 5 Minuten aus.

## Lizenz

Die Software ist lizenziert unter GNU GENERAL PUBLIC LICENSE Version 3. Mehr Informationen finden Sie [hier] (https://github.com/cuckoosandbox/cuckoo/blob/master/LICENSE)
