import config
import logging

from helper.Singleton import Singleton

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String
from sqlalchemy import DateTime,Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import SQLAlchemyError


Base = declarative_base()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class Database(object):

    __metaclass__ = Singleton

    def __init__(self):
        pass

    def connect(self,con_string=None):
        if not con_string:
            con_string = config.postgres['con']

        if not con_string:
            logger.warn("no connection to database!")
            return

        self.engine = create_engine(con_string, echo=False)
        self.Session = sessionmaker()
        self.Session.configure(bind=self.engine)

        session = self.Session()

        try:
            Base.metadata.create_all(self.engine)
            self.init_whitelists()
        except SQLAlchemyError as e:
            logger.warn("unable to create or connect to database: {}".format(e))
        finally:
            session.close()

    def init_whitelists(self):
        self.add_ipwl()
        self.add_domain_wl()
        self.add_process_wl()
        self.add_scoring_table()
        self.add_virustotal_table()

    def has_analyzed(self,hash):
        session = self.Session()
        try:
            for object in session.query(HashMapObject).filter(HashMapObject.hash.in_([hash])):
                if object:
                    return True
            return False
        except SQLAlchemyError as e:
            logger.debug("Database Error: {}".format(e))
        finally:
            session.close()

    def get_path(self,hash):
        session = self.Session()
        try:
            for object in session.query(HashMapObject).filter(HashMapObject.hash.in_([hash])):
                if object:
                    return object.path
        except SQLAlchemyError as e:
            logger.debug("Database error getting path: {}".format(e))
        finally:
            session.close()

    def select(self,category):
        session = self.Session()
        try:
            if category == 'process':
                result = session.query(ProcessWL).all()
            elif category == 'ip':
                result = session.query(IPWL).all()
            elif category == 'domain':
                result = session.query(DomainWL).all()

            result_list = [r for r in result]
            return result_list
        except SQLAlchemyError as e:
            logger.debug("Database error selecting: {}".format(e))
        finally:
            session.close()

    def get_score(self,category):
        session = self.Session()
        try:
            for object in session.query(Scoring).filter(Scoring.category==category):
                return object.score
            return 0
        except SQLAlchemyError as e:
            logger.debug("Database error getting score: {}".format(e))
            session.rollback()
        finally:
            session.close()

    def register_hash(self,hash,path_to_report):
        session = self.Session()
        object = HashMapObject(hash=hash,
                               path=path_to_report)
        if not self.has_analyzed(hash):
            session.add(object)

            try:
                session.commit()
            except SQLAlchemyError as e:
                logger.debug("Database error adding machine: {0}".format(e))
                session.rollback()
            finally:
                session.close()

    def isInReplyTable(self,filename,hash_payload,recipient_hash):
        session = self.Session()
        filename = filename if filename else ""

        try:
            for object in session.query(MailReplie).filter(MailReplie.filename.like('%{}%'.format(filename)),
                                                   MailReplie.mail_payloadhash.in_([hash_payload]),
                                                   MailReplie.recipient_hash.in_([recipient_hash])):
                return True
            return False
        except SQLAlchemyError as e:
            logger.debug("Database error querying replay table: {}".format(e))
        finally:
            session.close()

    def add_mail_reply(self,date_incoming_mail,date_of_analysis,recipient,recipient_hash,
                       filename,filehash,mail_payloadhash,score):
        session = self.Session()

        mail_replie = MailReplie(date_mail_incoming=date_incoming_mail,
                                 date_analysis=date_of_analysis,
                                 recipient=recipient,
                                 recipient_hash=recipient_hash,
                                 filename=filename,
                                 filehash=filehash,
                                 mail_payloadhash=mail_payloadhash,
                                 score=score)
        session.add(mail_replie)
        try:
            session.commit()
        except SQLAlchemyError as e:
            logger.debug("Database error adding machine: {0}".format(e))
            session.rollback()
        finally:
            session.close()

    def add_ipwl(self):
        if not self.engine.dialect.has_table(self.engine,"ip_wl"):
            logger.info("Error during DB Table initialization: ip_wl")
            return

        session = self.Session()

        result = session.query(IPWL).all()
        if len(result) > 0:
            return

        session.add_all([
            IPWL(ip="192.168.56.101"),
            IPWL(ip="192.168.56.102"),
            IPWL(ip="192.168.56.1"),
            IPWL(ip="192.168.56.255"),
            IPWL(ip="127.0.0.1")
        ])
        try:
            session.commit()
        except SQLAlchemyError as e:
            logger.debug("Database error adding machine: {0}".format(e))
            session.rollback()
        finally:
            session.close()

    def add_domain_wl(self):
        session = self.Session()

        result = session.query(DomainWL).all()
        if len(result) > 0:
            return

        session.add_all([
            DomainWL(name="www.msftncsi.com"),
            DomainWL(name="teredo.ipv6.microsoft.com"),
            DomainWL(name="isatap.dmz2.itsb.mpg.de"),
            DomainWL(name="_ldap._tcp.dc._msdcs.dmz2.itsb.mpg.de")
        ])

        try:
            session.commit()
        except SQLAlchemyError as e:
            logger.debug("Database error adding machine: {0}".format(e))
            session.rollback()
        finally:
            session.close()

    def add_process_wl(self):
        session = self.Session()

        result = session.query(ProcessWL).all()
        if len(result) > 0:
            return

        session.add_all([
            ProcessWL(path="C:\Windows\System32\lsass.exe",name="lsass.exe"),
            ProcessWL(path="C:\Program Files (x86)\Microsoft Office\Office14\WINWORD.EXE",name="WINWORD.EXE"),
            ProcessWL(path="C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe",name="AcroRd32.exe"),
            ProcessWL(path="C:\Program Files (x86)\Microsoft Office\Office14\EXCEL.EXE",name="EXCEL.EXE"),
            ProcessWL(path="C:\Program Files (x86)\Microsoft Office\Office14\POWERPNT.EXE",name="POWERPNT.EXE"),
            ProcessWL(path="C:\Windows\explorer.exe",name="explorer.exe"),
            ProcessWL(path="C:\Windows\splwow64.exe",name="splwow64.exe"),
            ProcessWL(path="C:\Program Files (x86)\Adobe\Reader 9.0\Reader\AcroRd32.exe",name="AcroRd32.exe"),
            ProcessWL(path="C:\Program Files (x86)\Java\jre1.8.0_161\bin\java.exe",name="java.exe"),
            ProcessWL(path="C:\Users\adminmps\AppData\Roaming\Oracle\bin\javaw.exe",name="javaw.exe")
        ])

        try:
            session.commit()
        except SQLAlchemyError as e:
            logger.debug("Database error adding machine: {0}".format(e))
            session.rollback()
        finally:
            session.close()

    def add_scoring_table(self):
        session = self.Session()

        result = session.query(Scoring).all()
        if len(result) > 0:
            return

        session.add_all([
            Scoring(category="yara",score=1.0),
            Scoring(category="domain",score=0.4),
            Scoring(category="udp",score=1.0),
            Scoring(category="tcp",score=1.0),
            Scoring(category="opened_files",score=0.3),
            Scoring(category="written_files",score=0.3),
            Scoring(category="moved_files",score=0.3),
            Scoring(category="deleted_files",score=0.3),
            Scoring(category="dir_enum",score=0.3),
            Scoring(category="process",score=2.0),
            Scoring(category="string",score=0.5),
            Scoring(category="host",score=0.4),
            Scoring(category="sig",score=0.5)
        ])

        try:
            session.commit()
        except SQLAlchemyError as e:
            logger.debug("Database error adding machine: {0}".format(e))
            session.rollback()
        finally:
            session.close()

    def get_virustotal_score(self,engine):
        session = self.Session()
        try:
            for object in session.query(Virustotal).filter(Virustotal.engine.in_([engine])):
                if object:
                    return object.score
            return 0
        except SQLAlchemyError as e:
            logger.debug("Database error getting virustotal score: {}".format(e))
            session.rollback()
        finally:
            session.close()

    def add_virustotal_table(self):
        session = self.Session()

        result = session.query(Virustotal).all()
        if len(result) > 0:
            return

        session.add_all([
            Virustotal(engine="alyac",score=1),Virustotal(engine="avware",score=2),
            Virustotal(engine="avg",score=2),Virustotal(engine="avira",score=2),
            Virustotal(engine="arcabit",score=1),Virustotal(engine="bkav",score=1),
            Virustotal(engine="totaldefense",score=2),Virustotal(engine="nprotect",score=1),
            Virustotal(engine="cmc",score=1),Virustotal(engine="mcafee",score=2),
            Virustotal(engine="malwarebytes",score=2),Virustotal(engine="zyllya",score=1),
            Virustotal(engine="aegislab",score=1),Virustotal(engine="k7antivirus",score=1),
            Virustotal(engine="alibaba",score=1),Virustotal(engine="k7gw",score=1),
            Virustotal(engine="thehacker",score=1),Virustotal(engine="baidu",score=1),
            Virustotal(engine="symantec",score=2),Virustotal(engine="avast",score=2),
            Virustotal(engine="clamav",score=2),Virustotal(engine="gdata",score=2),
            Virustotal(engine="kaspersky",score=2),Virustotal(engine="bitdefender",score=2),
            Virustotal(engine="superantispyware",score=1),Virustotal(engine="tencent",score=1),
            Virustotal(engine="emsisoft",score=1),Virustotal(engine="comodo",score=1),
            Virustotal(engine="drweb",score=1),Virustotal(engine="vipre",score=1),
            Virustotal(engine="sophos",score=2),Virustotal(engine="cyren",score=1),
            Virustotal(engine="jiangmin",score=1),Virustotal(engine="trendmicro",score=2),
            Virustotal(engine="kingsoft",score=1),Virustotal(engine="virobot",score=1),
            Virustotal(engine="microsoft",score=2),Virustotal(engine="vba32",score=1),
            Virustotal(engine="zoner",score=1),Virustotal(engine="rising",score=1),
            Virustotal(engine="yandex",score=1),Virustotal(engine="ikarus",score=1),
            Virustotal(engine="fortinet",score=1),Virustotal(engine="panda",score=1),
            Virustotal(engine="microworld-escan",score=1),Virustotal(engine="cat-quickheal",score=1),
            Virustotal(engine="f-prot",score=1),Virustotal(engine="trendmicro-housecall",score=1),
            Virustotal(engine="nano-antivirus",score=1),Virustotal(engine="ad-aware",score=1),
            Virustotal(engine="f-secure",score=1),Virustotal(engine="mcafee-gw-edition",score=2),
            Virustotal(engine="antiy-avl",score=1),Virustotal(engine="qihoo-360",score=1),
            Virustotal(engine="crowdstrike",score=1),
        ])

        try:
            session.commit()
        except SQLAlchemyError as e:
            log.debug("Database error adding machine: {0}".format(e))
            session.rollback()
        finally:
            session.close()

    def drop_all(self):
        con_string = config.postgres['con']
        self.engine = create_engine(con_string,echo=False)

        HashMapObject.__table__.drop(self.engine)
        IPWL.__table__.drop(self.engine)
        ProcessWL.__table__.drop(self.engine)
        DomainWL.__table__.drop(self.engine)
        Scoring.__table__.drop(self.engine)
        Virustotal.__table__.drop(self.engine)
        #MailReplie.__table__.drop(self.engine)

        logger.info( "Dropped all tables successfully" )

class HashMapObject(Base):
    __tablename__ = "hashmap_table"

    id = Column(Integer, primary_key=True)
    hash = Column(String)
    path = Column(String)


class IPWL(Base):
    __tablename__ = "ip_wl"

    id = Column(Integer, primary_key=True)
    ip = Column(String)


class ProcessWL(Base):
    __tablename__ = "process_wl"

    id = Column(Integer, primary_key=True)
    path = Column(String)
    name = Column(String)

class DomainWL(Base):
    __tablename__ = "domains_wl"

    id = Column(Integer, primary_key=True)
    name = Column(String)

class Scoring(Base):
    __tablename__ = "scoring"

    id = Column(Integer, primary_key=True)
    category = Column(String)
    score = Column(Float)

class Virustotal(Base):
    __tablename__ = "virustotal"

    id = Column(Integer, primary_key=True)
    engine = Column(String)
    score = Column(Float)

class MailReplie(Base):
    __tablename__ = "reply_table"

    id = Column(Integer, primary_key=True)
    date_mail_incoming = Column(DateTime)
    date_analysis = Column(DateTime)
    recipient = Column(String)
    recipient_hash = Column(String)
    filename = Column(String)
    filehash = Column(String)
    mail_payloadhash = Column(String)
    score = Column(String)
