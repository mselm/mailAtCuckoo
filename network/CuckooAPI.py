import os
import requests
import json
import ConfigParser
import time
import config
import logging


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

REST_URL = config.rest['create_file']
REST_URL_TASKS_REPORT = config.rest['tasks_report']
REST_URL_GET_PCAP = config.rest['get_pcap']
REST_URL_CUCKOO_STATUS = config.rest['cuckoo_status']
REST_URL_REPORT_STATUS = config.rest['report_status']
REST_URL_MACHINES_VIEW = config.rest['machines_view']
BASE_DIR =  config.cuckoo['attachment_path']


class CuckooAPI:

    @classmethod
    def send_to_cuckoo(cls, filename):
        FILE_PATH = os.path.join(BASE_DIR,filename)
        logger.debug("File sent to Cuckoo: {}".format(FILE_PATH))
        with open(FILE_PATH, "rb" ) as sample:
            multipart_file = {"file": (filename, sample)}
            request = requests.post(REST_URL, files=multipart_file)
            json_decoder = json.JSONDecoder()
            task_id = json_decoder.decode(request.text)["task_id"]
        time.sleep(5)
        return task_id

    @classmethod
    def getReport(cls, taskId):
        URL = os.path.join(REST_URL_TASKS_REPORT, str(taskId))
        response = requests.get(URL)
        if(response.status_code == 200):
            jData = json.loads(response.content)
            return jData
        else:
            logger.warn('error by getting report for {}'.format(taskId))

    @classmethod
    def getReportStatus(cls, taskId):
        response = requests.get(os.path.join(REST_URL_REPORT_STATUS, str(taskId)))
        status = response.json()['task']['status']

        logger.info(" TaskId: {}, report_status: {}".format(taskId, status))
        if status != 'reported':
            return False

        logger.info(" Free diskspace: {}MB available".format((cls.getFreeDiskspace()/1024)/1024))
        time.sleep(5)
        return True

    @classmethod
    def getFreeDiskspace(cls):
        response = requests.get(REST_URL_CUCKOO_STATUS)
        if(response.status_code == 200):
            jData = json.loads(response.content)
            freeDiskSpace= jData['diskspace']['analyses']['free']
            return freeDiskSpace
