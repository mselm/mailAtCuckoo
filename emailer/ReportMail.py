import config

from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart


STATUS_CODE_TABLE = dict()
STATUS_CODE_TABLE[0]="OK"
STATUS_CODE_TABLE[1]="Keine Datei zum Analysieren gefunden. Bitte f&uuml;gen Sie einen Anhang hinzu."
STATUS_CODE_TABLE[2]=("Der Dateityp wird nicht unterst&uuml;tzt. Achten Sie bitte darauf, dass der Anh"
                     "ang einem der folgenden Dateiformate entspricht: .doc(x|m), .xls(x|m), .ppt(x|m"
                     "), .pdf, .zip, .exe, .py, .js, .file, .com, .msg, .eml oder .jar - <i>The filet"
                     "ype is not supported. Please make sure that the file matches one of the followi"
                     "ng formats: .doc(x|m), .xls(x|m), .ppt(x|m), .pdf, .zip, .exe, .py, .js, .file,"
                     " .com, .msg, .eml or .jar</i>")
STATUS_CODE_TABLE[3]=("Der Emailanhang &uuml;berschreitet die maximal unterst&uuml;tzte Dateigr&ouml;"
                      "&szlig;e. Achten Sie bitte darauf, dass jede Datei nicht gr&ouml;&szlig;er als"
                      " 10MB sein darf. - <i>The file exceeds the maximum supported file size. Please"
                      " make sure that each file is not larger than 10MB</i>")
STATUS_CODE_TABLE[6]="Unbekannter Fehler - <i>Undefined error</i>"
STATUS_CODE_TABLE[10]=("Die URL-Analyse unterstuetzt keine .zip-Dateien. Falls moeglich, senden Sie d"
                       "ie Datei als Mail-Anhang an das Analysetool - <i>The Url-analysis does not su"
                       "pport .zip-files. If possible send the zip file as mail-attachment to the ana"
                       "lysis tool.</i>")
STATUS_CODE_TABLE[20]=("Die e-Mail enthaelt zu viele valide Dateien fuer die Analyse. Das Analysetool"
                       " akzeptiert maximal 4 Dateien pro e-Mail - <i>The e-Mail contains too many va"
                       "lid files for analysis. The analysis tool accepts a maximum of 4 files per em"
                       "ail.</i>")
STATUS_CODE_TABLE[70]=("Fehler beim Schreiben der Datei. M&ouml;glicherweise befindet sich ein Verzei"
                       "chnis innerhalb einer zip-Datei. Bitte senden Sie zur Analyse nur einzelne Da"
                       "teien.")
STATUS_CODE_TABLE[80]=("Wegen einer fehlerhaften Dateistruktur konnte die .zip-Datei nicht ausgepackt"
                       " werden - <i>BadZipFile-Error: Because of a incorrect zipfile-structure the f"
                       "ile cannot be unpacked.</i>")
STATUS_CODE_TABLE[90]=("Die .zip-Datei konnte nicht ge&ouml;ffnet werden. Bitte entfernen Sie den Pas"
                       "swortschutz bzw. &auml;ndern Sie das .zip-Passwort in 'infected' - <i>The zip"
                       "-File can not be opened. Please remove the password-protection or change the "
                       "zip-password to 'infected'.</i>")
STATUS_CODE_TABLE[95]=("Es ist ein Problem mit der Auswertung des Analyseergebnisses und/oder mit der"
                       " Bewertung des Ergebnisses aufgetreten. Versuchen Sie es erneut.")
STATUS_CODE_TABLE[99]=("Interner Fehler. Bitte versuchen Sie es sp&auml;ter nochmal. - <i>Internal er"
                       "ror. Please try again later.</i>")

class ReportMail:


    def __init__(self):
        pass

    def create_report_header_html(self):
        header = ""
        header += "<div style='background-color:#4279bb; padding: 5px 0px 5px 25px;'>"
        header +=     "<span style='font-weight:bold; font-size:1.1em;'>&nbsp;</span>"
        header += "</div>"
        header += "<div style='background-color:#dbdbdb;'>"
        header +=     "<br>"
        header +=     "&nbsp;&nbsp;<img src='cid:cuckoo_logo' align='center'>"
        header +=     "<span style='font-weight:bold; font-size:1.1em;'>&nbsp;</span>"
        header +=     "<br>"
        header += "</div>"
        return header

    def create_non_analysis_body(self, file, links):
        body = ""
        body += "<div>"

        body += self.create_report_header_html()

        body +=     "<div>"
        body +=         "<br>"

        if links:
            body += ("<span style='font-size:1.5em; padding-left: 30px;'>&nbsp;&nbsp;&nbsp;&nbsp;<b>I"
                     "nformationen zur Cuckoo Analyse</b></span> - <span style='font-size:1.5em'><i>I"
                     "nformation about the cuckoo analysis!</i><br><br></span>")

            malicious = False
            for url in links:
                if url.malicious:
                    malicious = True
                    break

            if malicious:
                body += "Die eingereichte eMail enth&auml;lt eine oder mehrere URL`s ("
                body += "Internetlinks), die als malizi&ouml;s eingestuft wurden. Bitte l&ouml;schen Sie die "
                body += "eMail und bleiben Sie weiter aufmerksam.<br>"
                body += "<i>The submitted eMail contains one or more URL's (internetlinks) that have been "
                body += "classified as malicious. Please delete the eMail and stay alert.</i>"
                body += "<br><br>"
                body += "<span style='color:#b70909; font-weight:bold;'>"
                body +=     "    Es wurden Malware-Signaturen gefunden -<br>"
                body +=     "    <i>Malware signatures were found</i>"
                body += "</span>"

            else:
                body += "Die eingereichte eMail enth&auml;lt ausschlie&szlig;lich URL`s, "
                body += "deren Inhalte mit diesem Analysetool leider nicht analysiert werden k&ouml;nnen."
                body += " Geben Sie bitte keinesfalls pers&ouml;nliche Daten ein, wenn Sie nicht &uuml;berzeug"
                body += "t sind von der Seri&ouml;st&auml;t der Website. Wenn Sie unsicher sind, nehmen Sie"
                body += " &uuml;ber einen anderen Kommunikationskanal Kontakt zum Absender auf, zum Beispie"
                body += "l telefonisch."
                body += "<br>"
                body += "Wenden Sie sich bei weiteren Fragen an Ihren Ansprechpartner f&uuml;r IT-Sicherheit."
                body += "<br><br>"
                body += "<i>The submitted eMail contains only URL's whose contents are unfortunately"
                body += " not analyzed with this analysis tool.<br>"
                body += "Please du not enter any personal data if you are not convined by the websites "
                body += "quality. If you are unsure, contact the sender via another communication channel,"
                body += "for example by phone."
                body += "<br>"
                body += "If you have further questions, please contact your local IT security staff.</i>"
                body += "<br><br>"

                body += "<span style='color:#b6ba12; font-weight:bold;'>"
                body +=     "    Keine Analyse auf Malware-Signaturen m&ouml;glich<br>"
                body +=     "    <i>No malware analysis possible</i>"
                body += "</span>"

        elif file:
            body += ("<span style='font-size:1.5em; padding-left: 30px;'>"
                         "&nbsp;&nbsp;&nbsp;&nbsp;<b>Leider ist ein Fehler bei der Analyse aufgetreten!</b>"
                     "</span> -  "
                     "<span style='font-size:1.5em'>"
                         "<i>Unfortunately, an error occurred during the an"
                         "alysis!</i>"
                     "</span>")
            body +=  "<br>"
            body +=  "<br>"
            body +=  "Dateiname <i>filename</i>: <code style='color:#4279bb; font-size:1.1em;'>{}</code><br>".format(file.filename)
            body +=  "Ursache <i>reason</i>: <code style='color:#4279bb;font-size:1.1em'>{}</code>".format(STATUS_CODE_TABLE[file.error_status])
        else:
            body += ("<span style='font-size:1.5em; padding-left: 30px;'>"
                         "&nbsp;&nbsp;&nbsp;&nbsp;<b>Es wurde keine Datei/keine URL (=Internetlink) f&uuml;r "
                         "die Analyse gefunden</b>"
                     "</span> - "
                     "<span style='font-size:1.5em'>"
                         "<i>No file or internetlink was found for the analysis</i>"
                     "</span>")
            body +=  "<br>"
            body +=  "<br>"
            body += ("Bitte &uuml;berpr&uuml;fen Sie die f&uuml;r die Analyse vorgesehene eMail - <i>"
                     "Please check the eMail provided for the analysis</i><br>")
            body +=  "<br>"
            body += ("Hinweis: Bitte beachten Sie, dass das Analysetool daf&uuml;r optimiert ist, Date"
                     "ien aus eMail-Anh&auml;ngen und - soweit m&ouml;glich - URLs"
                     " zu analysieren. Beinhaltet eine eMail keine der beiden Komponenten"
                     ", dann erhalten Sie diese Meldung.<br>")
            body +=  "<br>"
            body +=  "Die M&ouml;glichkeit besteht weiterhin, dass es sich bei der eMail<br>"
            body += ("&nbsp;&nbsp;- um Phishing handelt: Dabei steht das Abgreifen Ihrer pers&ouml;nli"
                     "chen Daten im Vordergrund<br>")
            body += "&nbsp;&nbsp;- um Spam handelt<br>"
            body += "&nbsp;&nbsp;- um eine seri&ouml;se Nachricht handelt.<br>"
            body += "<br>"
            body += "Nehmen Sie im Zweifel &uuml;ber einen anderen Kanal (z. B. telefonisch) Kontakt zum Absender der eMail auf,"
            body += " oder wenden Sie sich f&uuml;r weitere Hilfe an die zust&auml;ndige Stelle.<br>"
            body += "<br>"
            body += ("<i>Note: Please consider that the analysistool has been optimized for analyzing "
                    "files from e-mail-attachments and - if possible - URLs (=internetlinks)."
                    "If the e-Mail either contains attachments nor internetlinks you will get this "
                    "message.<br>")
            body += "<br>"
            body += "The possibility further remains that the e-mail:<br>"
            body += ("&nbsp;&nbsp;- is a phishing attack (the attackers are focused on personal d"
                    "ata.<br>")
            body += "&nbsp;&nbsp;- is spam<br>"
            body += "&nbsp;&nbsp;- is a serious message.<br>"
            body += "<br>"
            body += "If you are uncertain about the eMail quality try to contact the sender via another channel or contact your local staff.</i>"

        body += "<br>"
        body += "<br><br>"
        body += "<br><br>"
        body += "<p style='font-size:0.7em;'>"
        body +=     "Sollten Sie Fragen zur Analyse oder den Ergebnissen haben, senden Sie eine E-Mail"
        body +=     " an <i>{}</i>".format(config.email["sys_admin"])
        body += "</p>"
        body += "</div>"

        return body


    def create_analysis_body(self, file):
        resultsMap = file.report_analyzer.results_map
        scoring_map = file.report_analyzer.scoring_map

        body = ""
        body += "<div>"

        body += self.create_report_header_html()

        body += "<div width='50%'>"
        body += "<br>"

        if hasattr(file,"url"):
            body += "<span style='font-size:3em; padding-left: 30px;'>"
            body +=     "&nbsp;&nbsp;&nbsp;&nbsp;URL-Reporting"
            body += "</span>"
        else:
            body += "<span style='font-size:3em; padding-left: 30px;'>"
            body +=     "&nbsp;&nbsp;&nbsp;&nbsp;FILE-Reporting"
            body += "</span>"
            body += "<span style='font-size:2em'>"
            body +=     "f&uuml;r <i>for</i> <span style='color:#4279bb;'>{}</span>".format(file.filename)
            body += "</span>"

        body += "<p style='text-align:left;'>"
        body +=     ("Die eingereichte E-Mail wurde mit dem Malware-Profi-Analysetool Cuckoo Sandbox a"
                     "uf Malwaresignaturen untersucht. Die nachfolgenden Informationen zeigen zusammen"
                     "gefasst verd&auml;chtige Aktivit&auml;ten, die durch die Ausf&uuml;hrung der hoc"
                     "hgeladenen Datei festgestellt worden sind.<br>"
                     "<i> The submitted email was examined by the professional analysis tool cuckoo sa"
                     "ndbox on malware signatures. The following information show the summarized resul"
                     "ts of suspicious activities during the execution of the transmitted file.</i><br>")
        body += "<br>"

        if resultsMap['strings']:
            if resultsMap['strings']['is_encrypted'] == 'False' or \
              (resultsMap['strings']['is_encrypted'] == 'True' and scoring_map['totalScore'] > 0):
                body += "<p>Erzielter Gesamt-Gefahrenscore <i>achieved danger points</i>:<br>"
                body += "<br>"
                body += "<span style='color:{}; font-size:3em;'>".format(self.get_score_color(scoring_map['totalScore']))
                body +=     "&nbsp;&nbsp;&nbsp;{}".format(scoring_map['totalScore'])
                body += "</span>"
                body += " von <i>of</i> 10Pts</p>"
                body += "</div>"

        if hasattr(file,"url"):
            body += ("Gefundene URL - <i>Found URL</i>: <code style='color:#427"
                     "9bb; font-size:1.1em;'>{}</code> (maskiert <i>masked</i>)<br>".format(self.mask_url(file.url)))
            body += ("Referenzierte Datei - <i>Referenced file</i>: <code style='color:#4279bb; font-"
                     "size:1.1em;'>{0}</code><br>".format(file.filename))

        if scoring_map['totalScore'] == 0:
            if resultsMap['strings']:
                if resultsMap['strings']['is_encrypted'] == 'True':
                    body += "<span style='color:#b70909;'>"
                    body +=     "Die gefundene Datei ist verschl&uuml;sselt und konnte leider nicht analysiert werden. <b>Falls Sie keine derartige Datei erwarten, stufen Sie die Datei als gef&auml;hrlich ein!</b> -<br>"
                    body +=     "<i>The founded file is encrypted and can not be anlayzed. <b>If you do not expect such a file, classify the file as dangerous!</b></i>"
                    body += "</span>"
                    body += "</div>"
                return body

            else:
                body += "<span style='color:#008437;font-weight:bold;'>"
                body +=     "Es konnten keine Malware-Signaturen gefunden werden -<br>"
                body +=     "<i>No malware signatures could be found</i>"
                body += "</span>"
                body += "</div>"
                return body

        body += "<span style='color:#b70909; font-weight:bold;'>"
        body +=     "Es wurden Malware-Signaturen gefunden -<br>"
        body +=     "<i>Malware signatures were found</i>"
        body += "</span>"
        body += "<br>"
        body += "<br>"
        body += "<hr align='left' width='30%'>"
        body += "<b style='font-size:1.4em'>Experteninfos - <i>Information for experts:</i></b>"

        body += "<br>"
        body += "<br>"
        body += "<b>Meta-Dateiinfos <i>meta-fileinfos</i>:</b><br>"
        body += "<code>&nbsp;&nbsp;MD5: {}<br>".format(resultsMap['meta']['md5'])
        body += "&nbsp;&nbsp;SHA-1: {}<br>".format(resultsMap['meta']['sha1'])
        body += "&nbsp;&nbsp;SHA-256: {}<br>".format(resultsMap['meta']['sha256'])
        #body += "&nbsp;&nbsp;SHA-512: {}<br>".format(resultsMap['meta']['sha512'])
        body += "<br>"
        body += "</code>&nbsp;&nbsp;Dateityp <i>filetype</i>: <code>{}</code>".format(resultsMap['meta']['type'])

        if hasattr(file,"encapsulated_zip"):
            if file.encapsulated_zip:
                body += "<br>"
                body += "<br>"
                body += "<span style='color:#c10b0b; font-weight:bold'>"
                body +=     ("Achtung: Die Datei war in mehreren Zip-Files verschachtelt, dies ist ei"
                             "n starker Indikator f&uuml;r Malware! - <i>Attention: The uploaded file"
                             " was encapsulated in several zip-container. This is a strong indicator "
                             "for malware</i>")
                body += "</span>"

        if resultsMap['yara']:
            body += "<br><br><b>Patternanalyse:</b><br>Yara-Patternanalyse: {}".format(self.convertListToHTML(resultsMap['yara']))

        if resultsMap['signatures']:
            body += "<br><br><b>Auff&auml;llige Operationen <i>conspicuous operations</i>:</b><br> {}".format(self.convertListToHTML(resultsMap['signatures']))

        if resultsMap['network']:
            body += "<p><b>Netzwerkanalyse <i>network analysis</i>:</b>"
            if 'connects_host' in resultsMap['network']:
                if resultsMap['network']['connects_host']:
                    body += "<br>Verbundene Hosts <i>connected hosts</i>: <br>{}".format(self.convertListToHTML(resultsMap['network']['connects_host']))
            if 'domains' in resultsMap['network']:
                if resultsMap['network']['domains']:
                    body += "<br>Genutzte Domains <i>used domains</i>: <br>{}".format(self.convertListToHTML(resultsMap['network']['domains']))
            if 'tcpUdpCon' in resultsMap['network']:
                if resultsMap['network']['tcpUdpCon']:
                    body += "<br>Aufgebaute Netzwerk-Verbindungen <i>established network connections</i>: <br>{}".format(self.convertListToHTML(resultsMap['network']['tcpUdpCon']))
            body += "</p>"

        if resultsMap['behavior']:
            body += "<p><b>Verhaltensanalyse <i>behavioral analysis</i>:</b>"
            if resultsMap['behavior']['process']:
                body += "<br>Ausgef&uuml;hrte <b>Prozesse</b> <i>executed processes</i>: <br>{}".format(self.convertListToHTML(resultsMap['behavior']['process']))
            if resultsMap['behavior']['file_opened']:
                body += "<br>Ge&ouml;ffnete Dateien <i>opened files</i>: <br>{}".format(self.convertListToHTML(resultsMap['behavior']['file_opened']))
            if resultsMap['behavior']['file_written']:
                body += "<br>Ver&auml;nderte Dateien <i>written files</i>: <br>{}".format(self.convertListToHTML(resultsMap['behavior']['file_written']))
            if resultsMap['behavior']['file_deleted']:
                body += "<br>Gel&ouml;schte Dateien <i>deleted files</i>: <br>{}".format(self.convertListToHTML(resultsMap['behavior']['file_deleted']))
            if resultsMap['behavior']['directory_enumerated']:
                body += "<br>Involvierte Verzeichnisse <i>used directories</i>: <br>{}".format(self.convertListToHTML(resultsMap['behavior']['directory_enumerated']))
            body += "</p>"

        if resultsMap['virustotal']:
            body += ("<p><b>VirusTotal Analyse <i>VirusTotal analysis</i>:</b><br>Das Analysetool send"
                     "et keine Dateien an VirusTotal-Server. Das Tool ruft mithilfe des Dateihashes An"
                     "alyseergebnisse von bereits durchgef&uuml;hrten Anaysen seitens Dritter ab - <i>"
                     "VirusTotal does not upload any files independently to the servers. It only retri"
                     "eves the results of analyzes already carried out.</i>")
            body += "<br><br>{}".format(self.convertListToHTML(resultsMap['virustotal']))
            body += "</p>"

        if resultsMap['strings']:
            if resultsMap['strings']['malwareIndicators']:
                body += "<p><b>Stringanalyse <i>string analysis</i>:</b>"
                body += "<br>Auff&auml;llige Strings <i>conspicuous strings</i>: <br>{}".format(self.convertListToHTML(resultsMap['strings']['malwareIndicators']))
                body += "</p>"

        if resultsMap['dropped']:
           body += "<p><b>Erzeugte Dateien <i>dropped files</i>:</b>"
           body += "<br>"
           body += "Die folgenden Dateien wurden durch die Ausf&uuml;hrung der Target-Datei (<code>{}</code>) erzeugt<br>".format(resultsMap['meta']['filename'])
           body += "<i>the following files were dropped during the execution of the uploaded file</i>: <br>{}".format(self.convertListToHTML(resultsMap['dropped']))
           body += "</p>"
        body += "<br>"
        body += "<br>"
        body += ("<p style='font-size:0.7em;'>Sollten Sie Fragen zur Analyse oder den Ergebnissen haben,"
                 "senden Sie eine E-Mail an <i>{}</i></p>".format(config.email["sys_admin"]))
        body += "</div>"
        return body

    """ Color Hex-codes for html output:
        #4279bb = blue
        #edcc12 = yellow
        #c10b0b = red
    """
    def get_score_color(self, score):
        if score < 2:
            return "#4279bb"
        elif score < 4:
            return "#edcc12"
        return "#c10b0b"

    def limit_list_length(self, liste):
        if isinstance(liste, list):
            return liste[:50]

    """ Mask 1/3 of malicious url string in output
    """
    def mask_url(self, url):
        range = int(len(url) * 0.33)
        url_substr = url[range:2*range]
        url = url.replace(url_substr, "[...]")
        return url.replace("http","hxxp")

    """ Change http to hxxp because of security issues
    """
    def mask_http(self,string):
        return string.replace("http","hxxp")

    def convertListToHTML(self, li):
        html=""
        if not li:
            return html

        for item in li:
            try:
                html+="<code>&nbsp;&nbsp;- {}</code><br>".format(item).encode('utf-8')
            except UnicodeEncodeError:
                print "emailer/reportmail: unicode encode error"
                continue
            except:
                print "emailer/reportmail: undefined error"
                continue
        return html

