import logging

from lib.date import format_date,get_date
from lib.strings import get_domain,get_md5
from lib.mail import build_mime_email,send_email
from db.database import Database
from emailer.Sample import Sample
from emailer.MailPayload import MailPayload
from emailer.ReportMail import ReportMail
from emailer.Sample import CODE_OK,CODE_MAX_FILE_COUNT_EXCEEDED


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class EMail:

    def __init__(self, date=None, received=None, fromm=None, to=None, subject=None):
        self.date = date
        self.received = received
        self.fromm = to
        self.to = fromm
        self.subject = self.subject if subject else "<Kein Betreff>"
        self.content_type = ""
        self.mail_payload = None
        self.file_objects = list()

    """ Better text/html than no text, but better
        text/plain than text/html due to parsing reasons. When E-Mail contains
        more than one payload (sometimes mail attachments are handled as
        payload), than add payload string to existing payload
    """
    def set_mail_payload(self, mail_payload):
        if not self.mail_payload:
            self.mail_payload = mail_payload
        elif not self.mail_payload.content_type == "plain":
            self.mail_payload = mail_payload
        else:
            self.mail_payload.payload += mail_payload.payload

    def add_fileobjects_behind_urls(self):
        if self.mail_payload.urls:

            for url in self.mail_payload.urls:
                url_object = url.get_referenced_file()

                """ Dont save fileobject if there already exists
                an object with same filename AND url. See __eq__-function """
                if url_object and not url_object in self.file_objects:
                    self.file_objects.append(url_object)

    def create_report_mail(self):
        report = ReportMail()
        if self.mail_payload:
            return report.create_non_analysis_body(None,self.mail_payload.urls)
        else:
            return report.create_non_analysis_body(None,None)

    def send_report(self):
        if self.file_objects:
            for fo in self.file_objects:
                self.send_and_register_mail(fileobject=fo)
        else:
            self.send_and_register_mail()

    def send_and_register_mail(self,fileobject=None):
        db = Database()
        was_sent = False
        md5 = self.mail_payload.to_md5() if self.mail_payload else "n/a"

        if fileobject:
            was_sent = db.isInReplyTable(fileobject.filename,md5,get_md5(self.fromm))
            if was_sent:
                logger.info("Reportmail was still sent")
                return

            mail = fileobject.create_report_mail()
        else:
            was_sent = db.isInReplyTable("",md5,get_md5(self.fromm))

            if was_sent:
                logger.info("Reportmail was still sent")
                return

            mail = self.create_report_mail()

        mime_mail = build_mime_email(mail,self.fromm,self.subject)

        if send_email(mime_mail):
            filename = fileobject.filename if fileobject else "n/a"
            filehash = fileobject.to_md5() if fileobject else "n/a"
            score = "n/a"

            if fileobject and fileobject.has_analyzed():
                score = fileobject.report_analyzer.scoring_map['totalScore']

            md5 = self.mail_payload.to_md5() if self.mail_payload else "n/a"
            db.add_mail_reply(self.date,str(get_date()),get_domain(self.fromm),
                get_md5(self.fromm),filename,filehash,md5,score)

            logger.info("Email successfully sent and registered")
        else:
            logger.info("Error while sending E-Mail")


    """ To save system ressources a maximum
    of four (legal) files are allowed per email
    """
    def check_valid_fileobject_length(self):
        if self.__count_fo_status_codes(CODE_OK) > 5:
            logger.warn("Maximum number of allowed fileobjects exceeded")
            self.reset_file_objects()

            """ Add dummy object to fileobject-list with further no analysis information  """
            fo = Sample.factory("")
            fo.set_error_status(CODE_MAX_FILE_COUNT_EXCEEDED)
            self.add_file_object(fo)

    def __count_fo_status_codes(self, status_code):
        counter = 0
        for file in self.file_objects:
            if file.error_status == status_code:
                counter += 1
        return counter

    def reset_file_objects(self):
        self.file_objects = list()

    def add_file_object(self, fo):
        if fo not in self.file_objects:
            self.file_objects.append(fo)

    def set_date(self, date):
        logger.info(date)
        self.date = format_date(date)

    def get_date(self):
        return self.date

    def set_received(self, received):
        self.received = received

    def set_fromm(self, fromm):
        self.fromm = fromm

    def set_to(self, to):
        self.to = to

    def set_subject(self, subject):
        self.subject = subject

    def set_content_type(self, content_type):
        self.content_type = content_type

    def get_file_objects(self):
        return self.mail_body.get_file_objects()

    def __eq__(self, other):
        if not isinstance(other, EMail):
            return False
        if not self.subject == other.subject and\
           not self.fromm == other.fromm and\
           not self.to == other.to and\
           not len(self.file_objects) == len(other.file_objects):
            return False
        return True
