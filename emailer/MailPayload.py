import sys
import re
import logging

from lib.strings import get_md5
from emailer.UrlInfo import Url
from HTMLParser import HTMLParser


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def find_urls(string, type=""):

    p = HTMLParser()
    cleaned_links = list()

    if string:
        links = re.findall( r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-~_@.&<+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string)

        if not links:
            return

        for link in links:
            if '0A' in link:
                pos = link.find('0A')
                link = link[:pos]

            if '>' in link:
                pos = link.find('>')
                link = link[:pos]

            if link.endswith(('.','>',']')):
                link = link[:-1]

            if link.endswith(('])')):
                link = link[:-2]

            if link.endswith(('%20')):
                link = link[:-3]

            if '<' in link:
                substr = link
                pos = link.find('<')
                link = link[:pos]
                substring = substr[pos:]

                if 'http' in substring:
                    logger.debug("http found in substring: {}".format(substring))
                    cleaned_links.extend(find_urls(substring))

            if link.lower().endswith(('.png','.jpg','.jpeg','.gif')):
                continue

            if 'schemas.microsoft.com/office/2004/12/omml' in link.lower() or \
               'www.w3.org' in link.lower():
                continue

            url = Url(link)

            """ filter duplicates """
            if not url in cleaned_links and len(cleaned_links) < 15:
                cleaned_links.append(url)

    return cleaned_links


class MailPayload:

    def __init__(self, payload, content_type):
        self.payload = payload
        self.content_type = content_type

    def search_and_save_urls(self):
        self.urls = find_urls(self.payload,self.content_type)

        if not self.urls:
            return

        for url in self.urls:
            url.analyze()

    def to_md5(self):
        return get_md5(self.payload)
