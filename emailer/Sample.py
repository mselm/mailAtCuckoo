import re
import os
import config
import logging
import zipfile
import ssl
import json
import urllib2

from report.analyzingModules.ReportAnalyzer import ReportAnalyzer
from emailer.ReportMail import ReportMail
from lib.file import get_md5
from lib.strings import is_ascii,decode_string2,rename_file
from db.database import Database


CODE_OK = 0
CODE_NO_FILE_FOUND = 1
CODE_FILETYPE_NOT_SUPPORTED = 2
CODE_MAX_FILE_SIZE_EXCEEDED = 3
CODE_UNKNOWN_ERROR = 6
CODE_NO_ZIP_SUPPORT = 10
CODE_MAX_FILE_COUNT_EXCEEDED = 20
CODE_IO_ERROR = 70
CODE_DAMAGED_FILE = 80
CODE_OPEN_ZIP_ERROR = 90
CODE_RESULT_ERROR = 95
CODE_INTERNAL_ERROR = 99

ATTACHMENT_DIR = config.cuckoo['attachment_path']

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def is_zip(filename):
    return True if filename.lower().endswith(('.zip')) else False

def is_legal_filetype(filename):
    return filename.lower().endswith(('.iqy', '.doc', '.docx','.docm', '.xls', '.xlsx', '.xlsm',
                           '.ppt', '.pptx', '.pptm', '.pdf','.zip', '.exe', '.py', '.js','.file', '.com','.jar'))

class Sample(object):

    @staticmethod
    def validate_file(base_dir):
        base_dir = os.path.join(ATTACHMENT_DIR,base_dir)
        if not os.path.isfile(base_dir):
            logger.debug("No file found at: {}".format(base_dir))
            return CODE_NO_FILE_FOUND

        if os.path.getsize(base_dir) > 1024*1024*10*1.5:
            logger.debug("Maximum filesize of 15MB exceeded.: {}".format(os.path.getsize(base_dir)))
            return CODE_MAX_FILE_COUNT_EXCEEDED

        if not is_legal_filetype(base_dir):
            logger.debug("Filetype not supported: {}".format(base_dir))
            return CODE_FILETYPE_NOT_SUPPORTED

        return CODE_OK

    @staticmethod
    def factory(filename,url=None):
        base_dir = os.path.join(ATTACHMENT_DIR,filename)
        val_code = Sample.validate_file(base_dir)

        if url: return UrlSample(filename,url)
        if is_legal_filetype(base_dir): return FileSample(filename)

        return FileSample(filename,status=val_code)


class FileSample(object):

    def __init__(self,filename,status=0):
        self.filename = filename
        self.taskId = 0
        self.report_analyzer = None
        self.error_status = status

        if is_zip(self.filename):
            self.encapsulated_zip = False

            """ If successfully decompressed, analyze file with cuckoo """
            if self.decompress(os.path.join(config.cuckoo['attachment_path'],self.filename)):
                self.set_error_status(CODE_OK)

    def execute_report_analyzer(self):
        self.report_analyzer.search_report()
        self.report_analyzer.calculate_scores()

        if not hasattr(self.report_analyzer,'scoring_map'):
           self.set_status_code(CODE_RESULT_ERROR)

    def set_taskId(self,taskId):
        self.taskId = taskId

    def set_error_status(self,code):
        self.error_status = code

    def to_md5(self):
        path = os.path.join(config.cuckoo['attachment_path'],self.filename)

        if path and os.path.isfile(path):
            return get_md5(path)
        else:
            return "n/a"

    def create_report_mail(self):
        report = ReportMail()
        if self.error_status == CODE_OK:
            return report.create_analysis_body(self)
        return report.create_non_analysis_body(self, None)

    def execute_report_analyzer(self):
        self.report_analyzer.analyze()

        if not hasattr(self.report_analyzer,'scoring_map'):
           self.set_error_status(CODE_RESULT_ERROR)

    def set_report(self, jData):
        if self.taskId:
            self.report_analyzer = ReportAnalyzer(jData)
        else:
            logger.debug("Error while setting report for {}:\nNo task_id available".format(self.filename))

    def load_report(self):
        if self.error_status == CODE_OK and self.report_analyzer:
            if hasattr(self.report_analyzer,'scoring_map'):
                logger.info("Report exists!")
                return

        logger.info("Try loading report for {}".format(self.filename))

        db = Database()
        md5 = get_md5(os.path.join(config.cuckoo['attachment_path'],
                                    self.filename))
        path = db.get_path(md5)

        if path:
            with open(path) as file:
                data = json.load(file)
                self.set_taskId(999)
                self.set_report(data)
                self.execute_report_analyzer()
                logger.info("Successfully loaded report for {}".format(self.filename))

    def has_analyzed(self):
        if hasattr(self,"report_analyzer"):
            if hasattr(self.report_analyzer,"scoring_map"):
                return True
        return False

    def set_analyzing_status(self,status):
        self.analyzing_status = status

    def set_encapsulated_zip(self,bool):
        self.encapsulated_zip = bool

    """ Recursive unzip method """
    def decompress(self, filepath):
        try:
            with open(filepath, 'rb') as fh:
                if zipfile.is_zipfile(filepath):
                    z = zipfile.ZipFile(fh, "r" )
                    z.setpassword("infected")
                    name = z.namelist()

                    if not name:
                        self.set_error_status(CODE_INTERNAL_ERROR)
                        return False

                    for filename in name:
                        try:
                            data = z.read(filename)
                            new_path = os.path.join(config.cuckoo['attachment_path'],filename)
                            with open(new_path,"w") as ff:
                                ff.write(data)

                        except RuntimeError:
                            logger.debug("Runtime Error")
                            self.set_error_status(CODE_OPEN_ZIP_ERROR)
                            return False
                        except IOError:
                            logger.debug("IOError")
                            self.set_error_status(CODE_IO_ERROR)
                            return False
                        except:
                            logger.debug("Undefined error")
                            self.set_error_status(CODE_INTERNAL_ERROR)
                            return False

                        if is_zip(new_path):

                            """ Set flag for email-report """
                            self.set_encapsulated_zip(True)
                            return self.decompress(new_path)
                        else:
                            if not is_ascii(filename):
                                filename = rename_file(filename)

                            self.filename = filename
                            if not is_legal_filetype(filename):
                                self.set_error_status(CODE_FILETYPE_NOT_SUPPORTED)
                                return False

                            print self.filename
                            return True

                elif os.path.isdir(filepath):
                    logger.info( "Found directory in zip. Directories are not supported." )
                    return False
                elif is_zip(filepath):
                    self.set_error_status(CODE_DAMAGED_FILE)
                    logger.info( "ZipFile-Structure is damaged. File cannot be unpacked!" )
                    return False
                else:
                    return True
        except IOError:
            self.set_error_status(CODE_IO_ERROR)
            return False

    def unzip(self, filepath):
        return self.decompress(filepath)

    def __eq__(self, other):
        if not isinstance(other, FileSample):
            return False
        if not self.filename == other.filename:
            return False
        return True


class UrlSample(FileSample):

    def __init__(self,filename,url):
        super(UrlSample,self).__init__(filename)
        self.url = url
        if self.download_file_from_url():
            code = Sample.validate_file(self.filename)

            if code is not CODE_OK:
                self.set_error_status(code)
                return

            if is_zip(self.filename):

                self.encapsulated_zip = False

                if self.decompress(os.path.join(config.cuckoo['attachment_path'],self.filename)):
                    self.set_error_status(CODE_OK)
                    return
                else:
                    return

            self.set_error_status(CODE_OK)
        else:
            self.set_error_status(CODE_INTERNAL_ERROR)

    def download_file_from_url(self):

        """ Kill illegal chars in filename (See
        https://en.wikipedia.org/wiki/Filename) """
        self.filename = re.sub('[:/\\\?%*:|"<>]','_',self.filename)
        filepath = os.path.join(config.cuckoo['attachment_path'],self.filename)

        try:
            """ Create contextobject to disable certificate
            validation in case of https-connection """
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE

            """ Modify urllib-user agent to leave no traces from urllib """
            request = urllib2.Request(self.url,
                           headers={"User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0",
                                    "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"})

            response = urllib2.urlopen(request, context=ctx)

            with open(filepath, "wb") as fh:
                fh.write(response.read())

            if os.path.isfile(filepath):
                return True
            return False

        except:
            logger.debug("Error while downloading file")

    def __eq__(self, other):
        if not isinstance(other, UrlSample):
            return False
        return self.url == other.url\
            and self.filename == other.filename
