import config
import re
import urllib2
import random
import ssl
import logging
import socket

from virusTotal.VirusTotalObject import VirusTotalObject
from urlparse import urlparse
from Sample import Sample
from helper.MISP import MISP


RESPONSE_MAP = dict()
RESPONSE_MAP[200]=("Das Analysetool ist zur Analyse von Dateien entwickelt."
                   " Der Internetlink verweist jedoch auf eine textbasierte"
                   " Website, deren Inhalt nicht analysiert werden kann. <b>Ph"
                   "ishing-Webseiten fordern Sie h&auml;ufig dazu auf, (per"
                   "s&ouml;nliche) Daten einzugeben. Kommen Sie der Aufford"
                   "erung keinesfalls nach!</b> - <i>The ana"
                   "lysis-tool has been optimized for file-analysis, but th"
                   "e internetlink references a website with standard text "
                   "content. Avoid the input of (personal) data</i>")
RESPONSE_MAP[400]="Fehlerhafte Server-Anfrage - <i>Bad server request</i>"
RESPONSE_MAP[401]=("Die Anfrage kann nicht ohne gueltige Authentifizierung "
                   "durchgefuehrt werden - <i>authentication required.</i>")
RESPONSE_MAP[403]=("Es sind weitere Anmeldeinformationen erforderlich. Die "
                   "Website hat die Verbindung abgewiesen - HTTP Error 403:"
                   " Website requires further registration data")
RESPONSE_MAP[404]=("404: Die Website ist nicht (mehr) verf&uuml;gbar. Dies "
                   "liegt m&ouml;glicherweise daran, dass der Link falsch i"
                   "st oder aus Sicherheitsgr&uuml;nden deaktiviert wurde -"
                   " <i> This website is no longer available.</i>")
RESPONSE_MAP[500]=("HTTP Error. Kein Verbindungsaufbau m&ouml;glich - No co"
                  "nnection to website possible")
RESPONSE_MAP[501]=("HTTP Error. Kein Verbindungsaufbau m&ouml;glich - No co"
                  "nnection to website possible")
RESPONSE_MAP[502]=("Die Website ist nicht (mehr) verf&uuml;gbar - URLError:"
                   " This website is no longer available")

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Url(object):

    url_whitelist = ["www.facebook.com","www.facebook.de","www.youtube.com",
                     "microsoft.com","www.w3.org","www.o2online.de",
                     "www.gwdg.de","telefonica.de","twitter.com",
                     "linkedin.com","www.sz.de","www.faz.net","pki.pca.dfn.de",
                     "amazon.de","amazon.com"
    ]


    def __init__(self, tmp_link):
        self.tmp_link,self.link = tmp_link,tmp_link
        self.response = self.get_response()

        if self.response:
            self.link = self.response.geturl()

        self.status = ""
        self.malicious = False
        self.filename = None
        self.response_code = 0

    def analyze(self):

        """ If domain is part of whitelist, set status
        """
        if any(wl_entry in self.link.lower() for wl_entry in Url.url_whitelist):
            if not '@' in self.link.lower():
                self.status = "Ungef&auml;hrliche URL gefunden - <i>found harmless URL</i>"
                return

        mpg_domain = re.findall(r'http[s]?://.*mpg.de',self.link)

        if mpg_domain:
            if not '@' in mpg_domain[0]:
                self.status = "Ungef&auml;hrliche URL gefunden - <i>found harmless URL</i>"
                return

        self.filename = self.get_filename(self.response)

        if not self.filename:

            """ Virustotal query module
            """
            if not config.virustotal['key']:
                logger.info("No virustotal key found in config.py! For additional virustotal-analysis add key")
                return

            self.virustotal = VirusTotalObject(self.tmp_link,self.link)
            self.virustotal.check_infection_state()

            if self.virustotal.num_positives > 0 or self.virustotal.spam_list:
                self.status=("Achtung: Gef&auml;hrliche URL gefunden! - <i>"
                             "Attention: Found malicious URL!</i>")
                self.malicious = True

            """ misp module
            """
            if self.malicious:
                MISP().insert_url(self.link,self.get_IP_from_domain(self.get_TLD(self.link)))

    def set_status(self, response_code):
        self.set_response_code(response_code)
        self.status = RESPONSE_MAP[response_code]

    def get_referenced_file(self):
        if self.filename:
            return Sample.factory(self.filename,url=self.link)

    def set_response_code(self, code):
        self.response_code = code

    def get_TLD(self,link):
        parsed_uri = urlparse(link)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        if domain.endswith(('/')):
            domain = domain[:-1]

        return domain.split("//")[1]

    def get_IP_from_domain(self,domain):
        try:
            return socket.gethostbyname(domain)
        except:
            logger.info("Could not determine IP address")

    def get_response(self):
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        """ Hide user-agent:urllib in http-request header """
        request = urllib2.Request(self.tmp_link, headers={"User-Agent" :"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0",
                                                         "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"})
        try:
            response = urllib2.urlopen(request, context=ctx, timeout=35)
            self.set_status(response.getcode())
            return response

        except urllib2.HTTPError, e:
            if '40' in str(e.code):
                self.set_status(e.code)
            else:
                self.set_status(501)
        except urllib2.URLError:
            self.set_status(502)
        except socket.timeout:
            self.set_status(501)
        except:
            self.set_status(500)

    def get_filename(self,response):
        meta = None

        if isinstance(response, urllib2.addinfourl):
            meta = response.info()

        if meta:
            if 'Content-Disposition' in meta or re.findall( r'.*application.*' , str(meta)):
                if 'content-disposition' in meta:
                    content_disposition = meta['content-disposition']

                    """ Try to retrieve filename from response META info """
                    filename = re.findall( r'\".*\..{1,4}\"' , content_disposition)

                    if filename:
                        filename = filename[0][1:-1].strip()
                        return filename

                """ Try to retrieve filename from URL """
                filename = self.link.rsplit('/')[-1]
                filename = re.findall( r'.*\..{1,4}' , filename)

                if filename:
                    return filename[0].strip()
                else:
                    file_type = meta['Content-Type'].rsplit('/')[-1]
                    random_int = str(random.random()).rsplit('.')[-1]
                    return 'undefined_{}.{}'.format(random_int,file_type)

    def __eq__(self, other):

        if not isinstance(other, Url):
            return False

        return self.tmp_link == other.tmp_link
