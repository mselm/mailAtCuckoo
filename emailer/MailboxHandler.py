import binascii
import email
import config
import imaplib,smtplib
import os
import logging
import sys
import time
import re

from socket import error as SocketError
from lib.strings import decode_string
from lib.file import check_filetype_without_zip,is_zip
from emailer.MailPayload import MailPayload
from emailer.MailPayload import find_urls
from EMail import EMail
from emailer.Sample import Sample
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

ATTACHMENT_DIR = config.cuckoo['attachment_path']


class MailboxHandler:

    def __init__(self):
        self.msgs = None
        self.emails = list()

    def has_emails(self):
        if self.emails:
            return True
        return False

    def get_emails(self):
        return self.emails

    def get_msgs(self,con):
        if con:
            status, daten = con.search(None, "All")
            msgs = daten[0].split()
            return msgs
        else:
            sys.exit()

    def close_con(self,con):
        if not con:
            return

        try:
            con.close()
            con.logout()

        except SocketError:
            logger.warn("SocketError in close_connection (EmailHandler)")

    def cpy_and_del_msgs(self,con):
        for num in self.msgs:
            if config.email['backup_mailbox']:

                try:
                    status,result = con.copy(num,config.email['backup_mailbox'])

                    if status == 'OK' and result is not None:
                        logger.info("Copied message successfully to {}".format(config.email['backup_mailbox']))

                    else:
                        logger.info("Error copying message to {}\nCheck the config".format(config.email['backup_mailbox']))

                except:
                    logger.info("Error copying message to {}".format(config.email['backup_mailbox']))

            """ Set DELETE flag """
            con.store(num, '+FLAGS', '\\Deleted')

        """ Removes mails permanently from selected mailbox """
        con.expunge()

        logger.info("All Messages successfully deleted")

    def delete_backup_mailbox(self):
        con = self.connect(mb=config.email['mailbox_reported_mails'])
        msgs = self.get_msgs(con)

        for msg in msgs:

            """ Set DELETE flag """
            con.store(msg, '+FLAGS', '\\Deleted')

        """ Removes mails permanently from selected mailbox """
        con.expunge()
        logger.info("E-Mails in backup mailbox successfully deleted")

        self.close_con(con)

    def connect(self,host=config.email['imap_server'],username=config.email['username'],
                  pwd=config.email['password'],mb=config.email['mailbox'], counter=0):
        try:
            """ Max three tries to connect
            """
            if counter < 3:
                counter += 1
                time.sleep(5)
                con = imaplib.IMAP4_SSL(host)
                con.login(username,pwd)
                con.select(mb,readonly=False)
                return con
            else:
                logger.info("Could not connect to mailbox. Too many login attempts!")

        except imaplib.IMAP4.error:
            logger.info("Mailbox connection failed. Try reconnect")
            self.connect(counter=counter)

    def clean_mailbox(self):
        con = self.connect()
        self.cpy_and_del_msgs(con)
        self.close_con(con)

    """ MAIN DOWNLOAD METHOD
    """
    def download_emails_from_mailbox(self):
        con = self.connect()
        self.msgs = self.get_msgs(con)

        """ iterate through all founded emails """
        for uid in self.msgs:
            newmail = EMail()
            typ, s = con.fetch(uid, '(RFC822)')
            mail = email.message_from_string(s[0][1])

            newmail = self.set_email_meta(newmail,mail)
            self.disassemble_msg(newmail,mail)

            """ Do not add cuckoo reply emails
            """
            if not 'Cuckoo Analyse-Ergebnis' in newmail.subject:
                self.emails.append(newmail)

        self.close_con(con)

        for eemail in self.emails:
            if eemail.mail_payload:
                eemail.mail_payload.search_and_save_urls()
                eemail.add_fileobjects_behind_urls()


    def set_email_meta(self,newmail,mail):
        for part in mail.walk():
            fromm = part.get('From')

            if fromm and not newmail.fromm:
                newmail.set_date(part.get('Date'))
                newmail.set_fromm(part.get('From'))
                newmail.set_subject(decode_string(part.get('Subject')))
                return newmail

    """ Try to get file attachments and content subtypes html/plain
    """
    def disassemble_msg(self,newmail,mail):
        for part in mail.walk():
            filename = decode_string(part.get_filename())

            filename = re.sub('[:/\\\?%*:|"<> ]','_',filename)
            filename = filename.strip()

            """ Ignore cuckoo.png image in reply mail
            """
            if filename == "myimage":
                continue
            if part.get_content_maintype() == 'multipart':
                continue
            if filename:

                if not "." in filename:
                    continue

                """ dirty filetype filter for trash types
                """
                if filename.lower().endswith((".xml",".dat",".htm",".html",".jpg", ".png", ".p7s",".p7m",".p7c", ".bmp", ".gif")):
                    logger.info("found .xml/.dat/.htm/.jpg/.png/.p7[s|m|c]/.bmp/.gif -filetype. skip part.")
                    continue

                absolute_filepath = os.path.join(ATTACHMENT_DIR, filename)

                if filename.lower().endswith((".eml")):

                   if not os.path.isfile(absolute_filepath):
                       dump = part.get_payload(decode=True)

                       if not dump:
                           dump = part.get_payload()[0]

                       if not dump:
                           continue

                       with open(absolute_filepath, 'wb') as file:
                           if not isinstance(dump,basestring):
                               dump = dump.as_string()

                           file.write(dump)

                   eml_msg = None

                   with open(absolute_filepath) as fp:
                       eml_msg = email.message_from_file(fp)

                   self.disassemble_msg(newmail,eml_msg)

                """ If file doesnt exist: download to attachment folder """
                if not os.path.isfile(absolute_filepath):
                    with open(absolute_filepath, 'wb') as file:
                        file.write(part.get_payload(decode=True))

                if not filename.lower().endswith(('.eml')):
                    newmail.add_file_object(Sample.factory(filename))

                continue

            elif 'html' in part.get_content_subtype().lower()\
                or 'plain' in part.get_content_subtype().lower():
                charset = part.get_content_charset()

                if not charset:
                    charset = 'utf-8'

                encoding = part.get('Content-Transfer-Encoding')

                try:
                    payload = unicode(part.get_payload(decode=True),str(charset),"ignore").encode('utf8', 'replace')

                except LookupError:
                    payload =  part.get_payload(decode=True)

                if encoding:
                    if 'base64' in encoding and not find_urls(payload):
                        try:
                            payload = payload.decode('base64')

                        except binascii.Error:
                            payload = unicode(part.get_payload(decode=True),str(charset),"ignore").encode('utf8', 'replace')

                payload_ = MailPayload(payload,part.get_content_subtype().lower())

                if payload_:
                    newmail.set_mail_payload(payload_)

            else:
                continue

        #if newmail.mail_payload:
        #    newmail.mail_payload.search_and_save_urls()
        #    newmail.add_fileobjects_behind_urls()
