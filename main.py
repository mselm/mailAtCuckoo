###############################################################################
#                                                                             #
#               mailAtCuckoo is based on Cuckoo Sandbox rc 2.0.4              #
#             visit docs.cuckoosandbox.org/en/latest/installation/            #
#                                                                             #
###############################################################################
import sys
import os
import config
import logging
import argparse

from emailer.MailboxHandler import MailboxHandler
from lib.cleaner import Cleaner
from lib.network import is_analysis_running
from helper.Process import CuckooProcessManager
from datetime import datetime
from lib.analyzer.CuckooAnalyzer import CuckooAnalyzer
from db.database import Database
from helper.Virtualbox import VBInterface


logging.basicConfig(level=config.logging_level)
logger = logging.getLogger(__name__)


def main():

    parser = argparse.ArgumentParser(description='E-Mail Analysis Tool based on Cuckoo Sandbox.')
    parser.add_argument('--clean',help='Executes cuckoo clean command, DROPS all tables used by plugin and deletes all analysis related data.',
                       action="store_true")

    args = parser.parse_args()

    db = Database()
    mail_handler = MailboxHandler()

    """ Cleanup routine """
    if args.clean:
#        db.drop_all()

        mail_handler.delete_backup_mailbox()

        Cleaner.start_clearing()

        logger.info("Completely deleted all analysis related data successfully")
        sys.exit()

    db.connect()

    vb = VBInterface()

    CUCKOO_INTERFACE = config.resultserver['ip']

    logger.info("{0}: analysis has started".format(str(datetime.now())))

    """ If an analysis is in progress, stop executing """
    if is_analysis_running(CUCKOO_INTERFACE, 2042) or is_analysis_running('0.0.0.0', 5001):
        return

    with CuckooProcessManager():

        if not vb.check_machine_status("saved"):
            logger.warn("No available machine found")
            return

        mail_handler.download_emails_from_mailbox()

        if not mail_handler.has_emails():
            logger.info("No E-Mails found\n{}: analysis is finished".format(str(datetime.now())))
            return

        emails = mail_handler.get_emails()

        for email in emails:
            email.check_valid_fileobject_length()

        CuckooAnalyzer.execute_cuckoo_analyzer(emails)

        for email in emails:
             email.send_report()

        mail_handler.clean_mailbox()

    logger.info("{0}: analysis is finished".format(str(datetime.now())))

if __name__ == '__main__':
    main()
